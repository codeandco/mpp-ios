import Foundation
import XLPagerTabStrip
import SwiftEventBus

class GalleryTabController : ButtonBarPagerTabStripViewController {
    
    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        return [GalleryViewController()]
    }
    
    
    override func viewDidLoad() {
        
        settings.style.buttonBarBackgroundColor = Colors.red
        settings.style.buttonBarHeight = 50
        
    
        settings.style.buttonBarItemBackgroundColor = Colors.red
        settings.style.buttonBarItemTitleColor = UIColor.white
        
        settings.style.buttonBarItemLeftRightMargin = 20
        
        settings.style.selectedBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemsShouldFillAvailableWidth = false
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
        self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: size)
        
        
        
        SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
            let ev : TextSizeChange = result.object as! TextSizeChange
            self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: ev.textSize)
            
            self.buttonBarView.reloadData()
            
        }
        settings.style.selectedBarHeight  = 3

        
        super.viewDidLoad()
        
        
    }
}

