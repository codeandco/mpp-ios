import Foundation
import Kingfisher
import MaterialComponents
import SwiftEventBus
import XLPagerTabStrip
import SwiftyGif

class GalleryViewController : UITableViewController, IndicatorInfoProvider {
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Photos")
    }
    
    
    
    let UserCell = "ReceiptCell"
    var currentUsers: [Album] = [Album]()
    
    var textSize = UIFont.systemFontSize
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(GalleryCell.self, forCellReuseIdentifier: UserCell)
        
        
        let adjustForTabbarInsets = UIEdgeInsets(top: 0, left: 0, bottom: (self.tabBarController?.tabBar.frame)!.height, right: 0);
        self.tableView.contentInset = adjustForTabbarInsets;
        self.tableView.scrollIndicatorInsets = adjustForTabbarInsets;
        
        tableView.separatorStyle = .none
        
        self.view.backgroundColor = Colors.bg
        
        
        let gifmanager = SwiftyGifManager(memoryLimit:20)
        let gif = UIImage(gifName: "loader")
        let activityIndicator = UIImageView(gifImage: gif, manager: gifmanager)
        activityIndicator.frame =  CGRect(x: 0, y: 0, width: 15, height: 15)
        view.addSubview(activityIndicator)
        
        
        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.height.equalTo(80)
            
        }
        activityIndicator.startAnimating()
        
        
        //        let image = UIImage(named: "frame")
        //        let maskingImage = UIImage(named: "mask")
        //        maskImage(image: image!, mask: maskingImage!)
        //
        
        
        //        activityIndicator.snp.makeConstraints { (make) in
        //            make.center.equalTo(self.view)
        //        }
        //        activityIndicator.startAnimating()
        
        Api.getAlbums(completion: { (result) in
            print(result)
            self.currentUsers = result
            
            
            
            var i = 100;
            for res in self.currentUsers {
                res.tag = i
                i += 100
            }
            
            
            let textSize = UserDefaults.standard.value(forKey: "textSize")
            let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
            self.textSize = size
            
            
            self.tableView.reloadData()
            //            self.activityIndicator.stopAnimating()
            
            
            UIView.animate(withDuration: 0.3, animations: {
                activityIndicator.alpha = 0
            })
            
            SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
                let ev : TextSizeChange = result.object as! TextSizeChange
                self.textSize = ev.textSize
                self.tableView.reloadData()
                
                
                
            }
            
            
        })
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentUsers.chunk(withDistance: 2).count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screenSize = UIScreen.main.bounds
        return screenSize.height / 2.2
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.imageView?.kf.cancelDownloadTask()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Colors.bg
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UserCell)
            ?? UITableViewCell(style: .subtitle, reuseIdentifier: UserCell) as! GalleryCell
        
        //        let onlineUserEmail = currentUsers[indexPath.row]
        let onlineUserEmail = currentUsers.chunk(withDistance: 2)[indexPath.row]
        
        if( onlineUserEmail.count > 0) {
            (cell as! GalleryCell).eventName.text = "\(onlineUserEmail[0].albumName!)\n\(onlineUserEmail[0].year!)"
            (cell as! GalleryCell).eventName.sizeToFit()
            
            (cell as! GalleryCell).eventName.font = UIFont.systemFont(ofSize: textSize)
            (cell as! GalleryCell).eventName2.font = UIFont.systemFont(ofSize: textSize)
            
            let url = URL(string: onlineUserEmail[0].imageFullPath!)
            
            (cell as! GalleryCell).eventImage.kf.setImage(with: url,
                                                          placeholder: nil,
                                                          options: [.transition(.fade(1))],
                                                          progressBlock: nil,
                                                          completionHandler: nil)
            
            (cell as! GalleryCell).eventImage.tag = onlineUserEmail[0].tag!
            (cell as! GalleryCell).eventName.tag = onlineUserEmail[0].tag!
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            (cell as! GalleryCell).eventImage.isUserInteractionEnabled = true
            (cell as! GalleryCell).eventImage.addGestureRecognizer(tapGestureRecognizer)
            
            
            let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            (cell as! GalleryCell).eventName.isUserInteractionEnabled = true
            (cell as! GalleryCell).eventName.addGestureRecognizer(tapGestureRecognizer2)
            
            if( onlineUserEmail.count > 1) {
                (cell as! GalleryCell).eventName2.text = "\(onlineUserEmail[1].albumName!)\n\(onlineUserEmail[1].year!)"
                (cell as! GalleryCell).eventName2.sizeToFit()
                let url = URL(string: onlineUserEmail[1].imageFullPath!)
                
                
                (cell as! GalleryCell).eventImage2.kf.setImage(with: url,
                                                               placeholder: nil,
                                                               options: [.transition(.fade(1))],
                                                               progressBlock: nil,
                                                               completionHandler: nil)
                
                (cell as! GalleryCell).eventImage2.tag = onlineUserEmail[1].tag!
                (cell as! GalleryCell).eventName2.tag = onlineUserEmail[1].tag!
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                (cell as! GalleryCell).eventImage2.isUserInteractionEnabled = true
                (cell as! GalleryCell).eventImage2.addGestureRecognizer(tapGestureRecognizer)
                
                
                
                let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                (cell as! GalleryCell).eventName2.isUserInteractionEnabled = true
                (cell as! GalleryCell).eventName2.addGestureRecognizer(tapGestureRecognizer2)
                
            }
            
        }
        
        return cell
    }
    
    func maskImage(image:UIImage, mask:(UIImage))->UIImage{
        
        let imageReference = image.cgImage
        let maskReference = mask.cgImage
        
        let imageMask = CGImage(maskWidth: maskReference!.width,
                                height: maskReference!.height,
                                bitsPerComponent: maskReference!.bitsPerComponent,
                                bitsPerPixel: maskReference!.bitsPerPixel,
                                bytesPerRow: maskReference!.bytesPerRow,
                                provider: maskReference!.dataProvider!, decode: nil, shouldInterpolate: true)
        
        let maskedReference = imageReference!.masking(imageMask!)
        
        let maskedImage = UIImage(cgImage:maskedReference!)
        
        return maskedImage
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tag = tapGestureRecognizer.view!.tag
        
        for temp in currentUsers {
            
            if( temp.tag == tag) {
                
                GalleryDetailViewController.pKey = temp.pKey!
                GalleryDetailViewController.year = "\(temp.albumName!)"
                
                //                                let detail : CMFViewController = CMFViewController(nibName: "a", bundle: nil);
                //
                //
                //                                Api.getAlbumDetails(pkey: GalleryDetailViewController.pKey, completion: { (result) in
                //                //                    self.totalCount = result.count
                //                //                    self.currentPageLabel.text = "Photo \(1) of \(result.count)"
                //                //                    self.result = result
                //
                //                                    var array = NSMutableArray();
                //                                    for res in result {
                //                                        array.add(res.imageFullPath!)
                //                                    }
                //
                //                                    detail.dataArray = array.copy() as! [Any]
                //
                //
                //                                    self.addChildViewController(detail)
                //                                    self.view.addSubview(detail.view)
                //
                //                                    detail.didMove(toParentViewController: self)
                //
                //                //                       self.navigationController?.pushViewController(detail, animated: true)
                //                                })
                //
                //
                
                
                //                detail.loadImages()
                
                GalleryDetailViewController.textSize = self.textSize
                let controller = GalleryDetailViewController()
                //                self.addChildViewController(controller)
                //                self.view.addSubview(controller.view)
                //                controller.didMove(toParentViewController: self)
                
                
                
                
                navigationController?.pushViewController(controller, animated: true)
            }
        }
        
    }
    
}


