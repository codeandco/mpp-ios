import Foundation
import Kingfisher
import MaterialComponents
import SwiftyGif
import SwiftEventBus

class TvViewController : UITableViewController {
    
    public var newsCategory : String = "bn"
    let UserCell = "ReceiptCell"
    let UserCell2 = "ReceiptCell2"
    
    var sizeToFitCount = 0
    var textSize = UIFont.systemFontSize
    var currentUsers: [Article] = [Article]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.bg
        
        tableView.separatorStyle = .none
        tableView.register(TvCellHeader.self, forCellReuseIdentifier: UserCell2)
        tableView.register(TvCell.self, forCellReuseIdentifier: UserCell)
        
        self.view.backgroundColor = Colors.bg
        
        
        
        let gifmanager = SwiftyGifManager(memoryLimit:20)
        let gif = UIImage(gifName: "loader")
        let activityIndicator = UIImageView(gifImage: gif, manager: gifmanager)
        activityIndicator.frame =  CGRect(x: 0, y: 0, width: 15, height: 15)
        view.addSubview(activityIndicator)
        
        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.height.equalTo(80)
            
        }
        activityIndicator.startAnimating()
        
//        tableView.layoutMargins = .zero
        
        Api.getVideos(completion: { (result) in
            print(result)
            self.currentUsers = result
            
            
            
            
            activityIndicator.stopAnimating()
            
            let textSize = UserDefaults.standard.value(forKey: "textSize")
            let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
            self.textSize = size
            
            
            self.tableView.reloadData()
            
            UIView.animate(withDuration: 0.3, animations: {
                activityIndicator.alpha = 0
            })

                       
            SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
                let ev : TextSizeChange = result.object as! TextSizeChange
                self.textSize = ev.textSize
                self.tableView.reloadData()
                
                
                
            }
            

            
        })
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentUsers.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let screenSize = UIScreen.main.bounds
        
        if(indexPath.row == 0) {
            return screenSize.height / 2.5 + 60
            
        }
        
        return screenSize.height / 3.5 - 40
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.imageView?.kf.cancelDownloadTask()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Colors.bg
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (self.currentUsers[indexPath.row].website != nil) {
            UIApplication.shared.open(URL(string: self.currentUsers[indexPath.row].website!)!, options: [:])
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if(indexPath.row == 0) {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: UserCell2)
                ?? UITableViewCell(style: .subtitle, reuseIdentifier: UserCell2) as! TvCellHeader
            
            
            let onlineUserEmail = currentUsers[indexPath.row]
            
            (cell as! TvCellHeader).eventName.font = UIFont.systemFont(ofSize: textSize)
            
            if(  (cell as! TvCellHeader).eventName.font?.pointSize != textSize) {
                (cell as! TvCellHeader).eventName.sizeToFit()
            }

            (cell as! TvCellHeader).eventName.text =  onlineUserEmail.header
            
              (cell as! TvCellHeader).playImage.alpha = 1
        
            if(onlineUserEmail.imageTitle != nil) {
                let url = URL(string: onlineUserEmail.imageTitle!)
                (cell as! TvCellHeader).eventImage.kf.setImage(with: url,
                                                               placeholder: nil,
                                                               options: [.transition(.fade(0.5))],
                                                               progressBlock: nil,
                                                               completionHandler: {
                                                                (image, error, cacheType, imageUrl) in
                                                                
                                                                UIView.animate(withDuration: 0.1, animations: {
                                                                    
                                                                    (cell as! TvCellHeader).playImage.alpha = 0.6
                                                                })

                                                               
                                                                // image: Image? `nil` means failed
                                                                // error: NSError? non-`nil` means failed
                                                                // cacheType: CacheType
                                                                //                  .none - Just downloaded
                                                                //                  .memory - Got from memory cache
                                                                //                  .disk - Got from memory Disk
                                                                // imageUrl: URL of the image
                })
                
            }
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: UserCell)
                ?? UITableViewCell(style: .subtitle, reuseIdentifier: UserCell) as! TvCell
            
            
            let onlineUserEmail = currentUsers[indexPath.row]
            
            (cell as! TvCell).eventName.font = UIFont.systemFont(ofSize: textSize)
            
            if(  (cell as! TvCell).eventName.font?.pointSize != textSize) {
                (cell as! TvCell).eventName.sizeToFit()
            }

 
            (cell as! TvCell).eventName.text = onlineUserEmail.header
            
             (cell as! TvCell).playImage.alpha = 0
            
            if(onlineUserEmail.imageTitle != nil) {
                let url = URL(string:  onlineUserEmail.imageTitle!)
                (cell as! TvCell).eventImage.kf.setImage(with: url,
                                                         placeholder: nil,
                                                         options: [.transition(.fade(0.5))],
                                                         progressBlock: nil,
                                                         completionHandler: {
                    (image, error, cacheType, imageUrl) in
                    
                     
                                                            UIView.animate(withDuration: 0.1, animations: {
                                                                (cell as! TvCell).playImage.alpha = 0.6
                                                            })
                                      
                                                            
                    // image: Image? `nil` means failed
                    // error: NSError? non-`nil` means failed
                    // cacheType: CacheType
                    //                  .none - Just downloaded
                    //                  .memory - Got from memory cache
                    //                  .disk - Got from memory Disk
                    // imageUrl: URL of the image
                    })
                
            }
            
            sizeToFitCount = sizeToFitCount + 1
            return cell
        }
    }
    
}
