import Foundation
import Foundation
import MaterialCard
import UIKit
import SnapKit

class GalleryCell : UITableViewCell {
    
    public var eventName : UITextView = UITextView()
    public var eventName2 : UITextView = UITextView()
    
    var eventImage : UIImageView = UIImageView()
    var eventImage2 : UIImageView = UIImageView()
    
    var frame1 = UIImageView()
    var frame2 = UIImageView()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        self.contentView.addSubview(frame1)
        self.contentView.addSubview(frame2)

 
        
        self.contentView.addSubview(eventImage)
        self.contentView.addSubview(eventImage2)
        
        self.contentView.addSubview(eventName)
        self.contentView.addSubview(eventName2)
        selectionStyle = .none
        
        
        frame1.image = UIImage(named: "frame")
        frame2.image = UIImage(named: "frame")

        
        let purple = UIColor.black // 1.0 alpha
        let semi = purple.withAlphaComponent(0.5) // 0
        
        eventName.backgroundColor = semi
        eventName2.backgroundColor = semi
  
        eventImage.contentMode = UIViewContentMode.scaleAspectFill
        
        
        frame1.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(contentView)
            make.bottom.equalTo(contentView)
            make.left.equalTo(contentView)
            make.right.equalTo(contentView.snp.centerX).offset(-5)
            
        }

        
        eventImage.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(frame1).offset(0)
            make.bottom.equalTo(frame1).offset(-8)
            make.left.equalTo(frame1)
            make.right.equalTo(contentView.snp.centerX).offset(-10)
            
        }
        
        frame2.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(contentView)
            make.bottom.equalTo(contentView)
            make.left.equalTo(eventImage.snp.right).offset(5)
            make.right.equalTo(contentView)
            
        }

        
        eventImage2.contentMode = UIViewContentMode.scaleAspectFill
        eventImage2.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(frame2)
            make.bottom.equalTo(eventImage)
            make.left.equalTo(frame2)
            make.right.equalTo(contentView).offset(-5)
            
        }
        
        eventImage.clipsToBounds = true
        eventImage2.clipsToBounds = true
        
        
        eventName.snp.makeConstraints { (make) in
            make.bottom.left.right.equalTo(eventImage)
            make.height.lessThanOrEqualTo(contentView)
            
        }
        
        eventName2.text = "2017"
        
        eventName2.snp.makeConstraints { (make) in
            make.bottom.left.right.equalTo(eventImage2)
            make.height.lessThanOrEqualTo(contentView)
          }
        
        
        eventName.font = UIFont.boldSystemFont(ofSize: 14)
        eventName.textColor = UIColor.white
        eventName.isUserInteractionEnabled = false
        eventName.textContainer.maximumNumberOfLines = 4;
        eventName.textContainer.lineBreakMode = .byTruncatingTail;
        eventName.isEditable = false
        eventName.isSelectable = false
        eventName.isUserInteractionEnabled = false
        eventName.isScrollEnabled = false

        eventName2.font = UIFont.boldSystemFont(ofSize: 14)
        eventName2.textColor = UIColor.white
        eventName2.isUserInteractionEnabled = false
        eventName2.textContainer.maximumNumberOfLines = 4;
        eventName2.textContainer.lineBreakMode = .byTruncatingTail;
        eventName2.isEditable = false
        eventName2.isSelectable = false
        eventName2.isUserInteractionEnabled = false
        eventName2.isScrollEnabled = false
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(5, 10, 5, 10))
    }
    
}
