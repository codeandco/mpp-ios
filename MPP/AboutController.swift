import Foundation
import XLPagerTabStrip
import SwiftEventBus

class AboutController : UIViewController, IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "About MPP")
    }
    
    var didSetupConstraints = false
    
    let scrollView  = UIScrollView()
    let contentView = UIView()
    
    let logo = UIImageView()
    
    let aboutImageFrame = UIView()
    
    let label: UILabel = {
        let label = UILabel()

        label.numberOfLines = 0
        label.lineBreakMode = .byClipping
        label.textColor = .black

        label.attributedText = AboutController.stringFromHtml(string: "MPP-ME is a rapidly expanding, multi-faceted media company that offers creative media solutions to the luxury watch\n" + "\n" + "and jewellery industry in the Middle East.<br><br>" + "\n" + "Its established portfolio of publications includes flagship title Arabian Watches &amp; Jewellery Magazine, of which there\n" + "\n" + "are three independent editions: AWJ International, AWJ Kuwait and AWJ Levant.<br><br>" + "\n" + "MPP-ME was the first in the Middle East to launch a digitally distributed industry e-newsletter, which now has over\n" + "\n" + "24,000 subscribers via www.mpp-me.com - the region\'s leading watch and jewellery news website.<br><br><br><br>" + "\n" + "<b>The company publishes various other titles and offers corporate publishing services:</b><br><br>" + "\n" + "* Jewels of Arabia - annual fine jewellery coffee table book<br><br>" + "\n" + "* Times of Arabia - annual horologyre coffee table book<br><br>" + "\n" + "* PENME - the only high-end writing instrument magazine in the Middle East<br><br>" + "\n" + "* Official publishing partner of Jewellery Arabia exhibition, Bahrain &amp; Doha Jewellery &amp; Watch exhibition, Qatar<br><br>" + "\n" + "* World of Luxury, Asia Jewellers Bahrain magazine &amp; Lahazaat, Trafalgar Kuwait magazine<br><br><br>" + "\n" + "<b>MPP-ME is also responsible for some of the region\'s most prominent watch &amp; jewellery events:</b><br><br>" + "\n" + "* Middle East Jewellery of the Year awards for jewellery and writing instruments, held annually in Bahrain<br><br>" + "\n" + "* Middle East Watch of the Year awards held annually in Dubai, UAE<br><br>" + "\n" + "* Salon des Grandes Complications exhibition, held annually in Dubai, UAE<br><br>" + "\n" + "* Salon des Grandes Complications exhibition in Saudi Arabia, to be debut in 2017<br><br>" + "\n" + "* Collector gatherings for watch and jewellery companies")
   
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
        label.font = UIFont.systemFont(ofSize: size)
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        upImage.image = UIImage(named: "go_up")
        
        aboutImageFrame.backgroundColor = Colors.red
        
        view.backgroundColor = UIColor.white
        
        view.addSubview(scrollView)
        
        
        logo.image = UIImage(named: "logo")!.withRenderingMode(.alwaysTemplate)
        logo.tintColor = UIColor.white
        logo.contentMode = .scaleAspectFit
        
        contentView.backgroundColor = UIColor.white
        scrollView.addSubview(contentView)
        contentView.addSubview(label)
        contentView.addSubview(upImage)
        contentView.addSubview(aboutImageFrame)
        aboutImageFrame.addSubview(logo)

        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        upImage.isUserInteractionEnabled = true
        upImage.addGestureRecognizer(tapGestureRecognizer)
        
        view.setNeedsUpdateConstraints()
        
        
        SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
            let ev : TextSizeChange = result.object as! TextSizeChange
            self.label.font = UIFont.systemFont(ofSize: (ev.textSize))
        }
        
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        scrollView.scrollToTop()
    }
    
    override func updateViewConstraints() {
        
        if (!didSetupConstraints) {
            
            scrollView.snp.makeConstraints { make in
                make.edges.equalTo(view).inset(UIEdgeInsets.zero)
            }
            
            
            
          
            
            contentView.snp.makeConstraints { make in
                make.edges.equalTo(scrollView).inset(UIEdgeInsets.zero)
                make.width.equalTo(scrollView)
            }
            
            aboutImageFrame.snp.makeConstraints { make in
//                make.width.equalTo(contentView).multipliedBy(0.8)
                make.height.equalTo(80)
                
                make.leading.equalTo(contentView).inset(20)
                make.trailing.equalTo(contentView).inset(20)
                
                make.top.equalTo(contentView).inset(20)
                make.centerX.equalTo(contentView)
            }
            
            label.snp.makeConstraints { make in
                make.top.equalTo(aboutImageFrame.snp.bottom).offset(20)
                make.leading.equalTo(contentView).inset(20)
                make.trailing.equalTo(contentView).inset(20)
                make.bottom.equalTo(contentView).inset(100)
            }
            
            upImage.snp.makeConstraints { make in
                make.top.equalTo(label.snp.bottom).offset(20)
                make.centerX.equalTo(contentView)
                make.width.height.equalTo(60)
            }
            
            logo.snp.makeConstraints { make in
                make.left.equalTo(aboutImageFrame).inset(UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10))
                make.width.equalTo(aboutImageFrame).dividedBy(4)
                make.centerY.equalTo(aboutImageFrame)
            }
            
            didSetupConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    let upImage = UIImageView()
    
    
    static func stringFromHtml(string: String) -> NSAttributedString? {
        do {
            let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)
                
                return str
            }
        } catch {
        }
        return nil
    }
}


