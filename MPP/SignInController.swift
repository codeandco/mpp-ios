import Foundation
import UIKit
import SnapKit
import XLPagerTabStrip
import SwiftEventBus
import TwitterKit
import FacebookCore
import FacebookLogin


class SignInController : UIViewController, IndicatorInfoProvider, GIDSignInUIDelegate {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Sign in")
    }
    
    
    let image = UIImageView()
    
    let fbButton = UIButton()
    let twButton = UIButton()
    let gPlusButton = UIButton()
    var logInButton : TWTRLogInButton?
    let googleButton = GIDSignInButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self

        googleButton.alpha = 0
        
        view.addSubview(image)
        view.addSubview(fbButton)
        view.addSubview(twButton)
        view.addSubview(gPlusButton)
        view.addSubview(googleButton)

   
        fbButton.setImage(UIImage(named: "fb") , for: UIControlState.normal)
        fbButton.setTitle("   Login with Facebook" , for: UIControlState.normal)
        fbButton.backgroundColor = Colors.facebook
        fbButton.contentEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
        
        
       
        twButton.setImage(UIImage(named: "tw" ), for: UIControlState.normal)
        twButton.setTitle("   Login with Twitter" , for: UIControlState.normal)
        twButton.backgroundColor = Colors.twitter
        twButton.contentEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
        
        
        gPlusButton.setImage(UIImage(named: "gplus" ), for: UIControlState.normal)
        gPlusButton.setTitle("   Login with Google+" , for: UIControlState.normal)
        gPlusButton.backgroundColor = Colors.google
        gPlusButton.contentEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)

        image.image = UIImage(named: "login_bg")
        image.contentMode = .scaleAspectFill
        
        image.snp.makeConstraints { make in
            make.width.height.equalTo(self.view)
        }
       
    
        twButton.snp.makeConstraints { make in
            make.width.centerY.centerX.equalTo(self.view)
        }
        
        fbButton.snp.makeConstraints { make in
            make.width.equalTo(self.view)
            make.bottom.equalTo(twButton.snp.top).offset(-10)
        }
        
        gPlusButton.snp.makeConstraints { make in
            make.width.equalTo(self.view)
            make.top.equalTo(twButton.snp.bottom).offset(10)
        }
        
        
        twButton.addTarget(self, action:#selector(self.twitterClicked), for: .touchUpInside)
        fbButton.addTarget(self, action:#selector(self.facebookClicked), for: .touchUpInside)
        gPlusButton.addTarget(self, action:#selector(self.googlePlusClicked), for: .touchUpInside)
        
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
        self.twButton.titleLabel?.font = UIFont.systemFont(ofSize: size)
        self.fbButton.titleLabel?.font = UIFont.systemFont(ofSize: size)
        self.gPlusButton.titleLabel?.font = UIFont.systemFont(ofSize: size)

        
        
        SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
            let ev : TextSizeChange = result.object as! TextSizeChange
            self.twButton.titleLabel?.font = UIFont.systemFont(ofSize: (ev.textSize))
            self.fbButton.titleLabel?.font = UIFont.systemFont(ofSize: (ev.textSize))
            self.gPlusButton.titleLabel?.font = UIFont.systemFont(ofSize: (ev.textSize))
        }
        
        self.logInButton = TWTRLogInButton { (session, error) in
            if let unwrappedSession = session {
//                let alert = UIAlertController(title: "Logged In",
//                                              message: "User \(unwrappedSession.userName) has logged in",
//                    preferredStyle: UIAlertControllerStyle.alert
//                )
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
            } else {
                NSLog("Login error: %@", error!.localizedDescription);
            }
        }
        

        
        self.view.addSubview(logInButton!)
        
        logInButton!.backgroundColor = UIColor.white
        logInButton!.setTitle("   Login with Twitter" , for: UIControlState.normal)
        logInButton!.alpha = 0
        logInButton!.snp.makeConstraints { make in
            make.width.centerY.centerX.equalTo(self.view)
            make.height.equalTo(twButton)
        }
        
        
        
    }
    
    
    func twitterClicked() {
        print("Button Clicked")
        logInButton!.sendActions(for: .touchUpInside)

        
    }
    
    
    func facebookClicked() {
        print("Button Clicked")
        let loginManager = LoginManager()
        loginManager.logIn([.publicProfile], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
            }
        }
    }
    
    
    
    func googlePlusClicked() {
        print("Button Clicked")
        googleButton.sendActions(for: .touchUpInside)

    }
}
