import Foundation
import UIKit
import SnapKit
import Alamofire
import SwiftyGif
import SwiftEventBus


class SearchViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    var textSize = UIFont.systemFontSize
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    //    let searchController = UISearchController(searchResultsController: nil)
    let UserCell = "ReceiptCell"
    var currentUsers: [Article] = [Article]()
    var activityIndicator : UIImageView?
    var lastRequest : DataRequest?
    
    @IBOutlet var searchView: UIView!
    
    @IBOutlet weak var searchView2: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Colors.bg
        self.tableView.backgroundColor = Colors.bg
        
        
        //        searchBar.removeFromSuperview()
        
        searchView2.backgroundColor = Colors.bg
        
        let gifmanager = SwiftyGifManager(memoryLimit:20)
        let gif = UIImage(gifName: "loader")
        activityIndicator = UIImageView(gifImage: gif, manager: gifmanager)
        activityIndicator!.frame =  CGRect(x: 0, y: 0, width: 15, height: 15)
        searchView2.addSubview(activityIndicator!)
        
        
        activityIndicator!.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.height.equalTo(80)
            
        }
        activityIndicator!.startAnimating()
        searchView2.alpha = 0
        
        
        tableView.separatorStyle = .none
        tableView.register(NewsCell.self, forCellReuseIdentifier: UserCell)
        
        searchBar.barTintColor = Colors.red
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search"
        searchBar.subviews[0].subviews.flatMap(){ $0 as? UITextField }.first?.tintColor = UIColor.black

        let view: UIView = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        
        for subView: UIView in subViewsArray {
            if let cancelButt = subView as? UIButton{
                cancelButt.setTitleColor(UIColor.white, for: .normal)
                
            }
        }
        
        
        //        searchController.delegate = self
        //        searchController.searchBar.showsSearchResultsButton = true
        searchBar.showsCancelButton = false
        searchBar.showsCancelButton = true

        searchBar.delegate = self
        //        searchController.searchResultsUpdater = self
        //        dimsBackgroundDuringPresentation = false
        //        definesPresentationContext = true
        //        tableView.tableHeaderView = searchController.searchBar
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        //        view.addSubview(searchController.searchBar)
        
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
        self.textSize = size
        
        
        SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
            let ev : TextSizeChange = result.object as! TextSizeChange
            self.textSize = ev.textSize
            self.tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("search clicked ")
//        searchBar.resignFirstResponder()
//        searchBar.showsCancelButton = true
        
        
        filterContentForSearchText(searchText: searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //        filterContentForSearchText(searchText: searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        lastRequest?.cancel()
        searchBar.text = nil
        searchBar.resignFirstResponder()
        
        self.currentUsers = []
        self.tableView.reloadData()
        UIView.animate(withDuration: 0.3, animations: {
            self.searchView2.alpha = 0
        })
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        
        
        if(searchText.isEmpty) {
            return
        }
        
        searchView2.alpha = 1
        
        lastRequest?.cancel()
        
        Api.searchNews(search: searchText,  completion: { (request, result) in
            print(result)
            self.currentUsers = result
            self.tableView.reloadData()
            UIView.animate(withDuration: 0.3, animations: {
                self.searchView2.alpha = 0
            })
            
            self.lastRequest = request
            
            print("show results \(self.currentUsers.count)" )
            
            DispatchQueue.main.async { self.tableView.reloadData() }
            //            self.tableView.reloadData()
            
            
        })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentUsers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screenSize = UIScreen.main.bounds
        return screenSize.height / 3.5 - 40
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.imageView?.kf.cancelDownloadTask()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Colors.bg
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //                cell.backgroundColor = Colors.bg
        NewsDetailViewController.pKey = self.currentUsers[indexPath.row].pKey
        NewsDetailViewController.fullImage = true
        NewsDetailViewController.fullImagePath = self.currentUsers[indexPath.row].imageTitle!

        navigationController?.pushViewController(NewsDetailViewController(nibName: "NewsDetailView", bundle: nil), animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("table cell make")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UserCell)
            ?? UITableViewCell(style: .subtitle, reuseIdentifier: UserCell) as! NewsCell
        
        
        let onlineUserEmail = currentUsers[indexPath.row]
        (cell as! NewsCell).eventName.font = UIFont.systemFont(ofSize: textSize)
        //        (cell as! NewsCell).eventName.sizeToFit()
        
        
        if(  (cell as! NewsCell).eventName.font?.pointSize != textSize) {
            (cell as! NewsCell).eventName.sizeToFit()
        }
        
        
        (cell as! NewsCell).eventName.text = onlineUserEmail.header
        
        if(onlineUserEmail.imageTitle != nil) {
            let url = URL(string: onlineUserEmail.imageTitle!)
            
            
            (cell as! NewsCell).eventImage.kf.setImage(with: url,
                                                       placeholder: nil,
                                                       options: [.transition(.fade(1))],
                                                       progressBlock: nil,
                                                       completionHandler: nil)
            
        }
        return cell
    }
    
    
}


extension SearchViewController: UISearchResultsUpdating {
    
    
    
    
    @available(iOS 8.0, *)
    public func updateSearchResults(for searchController: UISearchController) {
        //         filterContentForSearchText(searchText: searchController.searchBar.text!)
        
    }
}
