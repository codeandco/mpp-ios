import Foundation


class PageChangedEvent {
    
    var page : Int

    init(page : Int) {
        self.page = page;
    }
    
}
