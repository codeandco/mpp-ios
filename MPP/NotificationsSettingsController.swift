import Foundation
import UIKit
import SnapKit
import XLPagerTabStrip
import SwiftEventBus


class NotificationsSettingsController : UIViewController, IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Notifications")
    }
    
    let titleLabel = UILabel()
    let subtitleLabel = UILabel()
    
    let useNotifications = UISwitch()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        self.view.addSubview(titleLabel)
        self.view.addSubview(subtitleLabel)
        self.view.addSubview(useNotifications)

        useNotifications.onTintColor = Colors.red
        

        titleLabel.text = "Notifications at night"
        titleLabel.font = UIFont.boldSystemFont(ofSize: titleLabel.font.pointSize)
        
        subtitleLabel.text = "(10:00 PM to 7:00 AM)"
        subtitleLabel.textColor = UIColor.black
        
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: size)
        self.subtitleLabel.font = UIFont.systemFont(ofSize: size)
        
        
        
        SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
            let ev : TextSizeChange = result.object as! TextSizeChange
            
            self.titleLabel.font = UIFont.boldSystemFont(ofSize: ev.textSize)
            self.subtitleLabel.font = UIFont.systemFont(ofSize: ev.textSize)
        }

        
 
        
        titleLabel.snp.makeConstraints { make in
            make.left.top.equalTo(self.view).inset(10)
        }

        
        subtitleLabel.snp.makeConstraints { make in
            make.left.equalTo(self.view).inset(10)
            make.top.equalTo(self.view).offset(40)
        }
        
        useNotifications.snp.makeConstraints { make in
         make.right.equalTo(self.view).inset(10)
            make.centerY.equalTo(titleLabel.snp.bottom)
        }
        
        let result = UserDefaults.standard.value(forKey: "notifications") as? Bool
        useNotifications.setOn(result == nil ? false : result!, animated: false)
        
        
        useNotifications.addTarget(self, action:#selector(self.switchChanged), for: .valueChanged)
        
    }
    
    
    func switchChanged() {
        let value = useNotifications.isOn
        UserDefaults.standard.set(value, forKey: "notifications")
    }
}
