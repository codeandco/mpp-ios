import Foundation
import Foundation
import MaterialCard
import UIKit
import SnapKit


class TvCellHeader : UITableViewCell {
    
    public var eventName: UITextView = UITextView()
    var eventImage : UIImageView = UIImageView()
    var card : MaterialCard = MaterialCard()
    let playImage = UIImageView()
    
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {

            var frame = newFrame
            frame.origin.x += 4
            frame.size.width -= 5
            super.frame = frame
            
//            let inset: CGFloat = 15
//            var frame = newFrame
//            frame.origin.x -= inset
//            frame.size.width += 2 * inset
//            super.frame = frame
        }
    }

    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        card.backgroundColor = UIColor.white
        card.shadowOffsetHeight = 2
        card.shadowOffsetWidth =  Int(card.frame.size.width)
        card.shadowColor = UIColor.black.withAlphaComponent(0.2)
        card.shadowOpacity = 0.25

 
        
        playImage.image = UIImage(named: "ic_play_circle_filled")
        
        
        
        self.contentView.addSubview(card)
        self.card.addSubview(eventName)
        self.card.addSubview(eventImage)
        self.card.addSubview(playImage)

        selectionStyle = .none
        
        
        card.snp.makeConstraints { (make) -> Void in
             make.width.equalTo(contentView)
            make.height.equalTo(contentView)
        }
        
        
        eventImage.contentMode = UIViewContentMode.scaleAspectFill
        eventImage.snp.makeConstraints { (make) -> Void in
             make.height.equalTo(contentView).multipliedBy(0.75)
            make.width.equalTo(contentView)
            
        }
        

        eventImage.clipsToBounds = true
        
        
        eventName.snp.makeConstraints { (make) in
            make.top.equalTo(eventImage.snp.bottom)
            make.width.equalTo(contentView).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10))
            make.height.equalTo(contentView).multipliedBy(0.25)
            make.left.equalTo(card).offset(10).priority(500)
            make.right.equalTo(card).offset(-10).priority(500)

        }
        
 
        
        playImage.snp.makeConstraints { (make) in
            make.center.equalTo(eventImage)
            make.width.height.equalTo(70)
        }
        
        playImage.alpha = 0.0
			
        
        eventName.font = UIFont.boldSystemFont(ofSize: 14)
        eventName.textColor = UIColor.black
        
        eventName.isEditable = false
        eventName.isSelectable = false
        eventName.isUserInteractionEnabled = false
        eventImage.isUserInteractionEnabled = false
        card.isUserInteractionEnabled = false
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(5, 10, 5, 10))
        
    }
    
}
