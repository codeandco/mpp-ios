import Foundation
import Alamofire
import SWXMLHash
import StringExtensionHTML
import AEXML
import SwiftMoment

class Country  {
    var name:String = ""
}

class Article {
    var sortOrder:String? = ""
    var newsSDesc:String? = ""
    var imageTitle:String? = ""
    var newsDesc : String? = ""
    var pKey : String? = ""
    var header : String? = ""
    var website : String? = ""
    var date : String? = ""
    
}


class Album  {
    var pKey : String? = ""
    var albumName : String? = ""
    var year : Int?
    var imageName : String? = ""
    var imageFullPath : String? = ""
    
    var tag : Int?
}

class NewsCategory {
    var newsTypeId : String? = ""
    var newsType : String? = ""
}

@objc class Api : NSObject {
    
    static let IMAGE_URL = "http://d2495236.u68.gohsphere.com/NewsImages/%@/"
    static let IMAGE_URL_VIDEO = "http://www.awjonlinetv.com/Videos/Images/";
    
    
    
    static func getNews(newsCategory : String , completion: @escaping (_ result: [Article]) -> Void) -> Void {
        
        
        var result = [Article]()
        let soapRequest = AEXMLDocument()
        let envelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema", "xmlns:soap12" : "http://www.w3.org/2003/05/soap-envelope"]
        let envelope = soapRequest.addChild(name: "soap12:Envelope", attributes: envelopeAttributes)
        let body = envelope.addChild(name: "soap12:Body")
        let child = body.addChild(name: "GetCategoryWiseNews",  attributes: ["xmlns" : "http://tempuri.org/"])
        child.addChild(name: "strNewsTypeID", value: newsCategory )
        
        print(soapRequest.xml)
        
        let soapLenth = String(soapRequest.xml.characters.count)
        let theURL = NSURL(string: "http://d2495236.u68.gohsphere.com/CategoryWiseNews.asmx")
        
        var mutableR = URLRequest(url: theURL! as URL)
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("text/html; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("utf-8", forHTTPHeaderField: "Accept-Charset")
        
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        Alamofire.request(mutableR)
            .responseString { response in
                if let xmlString = response.result.value {
                    
                    do {
                        let xmlDoc = try AEXMLDocument(xml: xmlString)
                        
                        if let content = xmlDoc.root["soap:Body"]["GetCategoryWiseNewsResponse"]["GetCategoryWiseNewsResult"]["diffgr:diffgram"]["NewDataSet"]["AllInitiatives"].all {
                        
                            for child in content {
                                
                                let article = Article()
                                article.imageTitle = child["ImageName"].value
                                article.pKey = child["PKey"].value
                                article.header = child["Header"].value
                                article.date = child["CreatedDate"].value
                                result.append(article)
                            }
                        }
                        
                        completion(result)
                    } catch {
                        print("\(error)")
                    }
                }
        }
    }
    
    static func searchNews(search : String , completion: @escaping (_ request : DataRequest, _ result: [Article]) -> Void) -> Void {
        
        
        var result = [Article]()
        let soapRequest = AEXMLDocument()
        let envelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema", "xmlns:soap12" : "http://www.w3.org/2003/05/soap-envelope"]
        let envelope = soapRequest.addChild(name: "soap12:Envelope", attributes: envelopeAttributes)
        let body = envelope.addChild(name: "soap12:Body")
        let child = body.addChild(name: "GetSearchNews",  attributes: ["xmlns" : "http://tempuri.org/"])
        child.addChild(name: "strText", value: search )
        
        print(soapRequest.xml)
        
        let soapLenth = String(soapRequest.xml.characters.count)
        let theURL = NSURL(string: "http://d2495236.u68.gohsphere.com/searchnews.asmx")
        
        var mutableR = URLRequest(url: theURL! as URL)
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("text/html; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("utf-8", forHTTPHeaderField: "Accept-Charset")
        
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        
        var request : DataRequest?
        request = Alamofire.request(mutableR)
            .responseString { response in
                if let xmlString = response.result.value {
                    
                    
                    var counter = 200
                    do {
                        let xmlDoc = try AEXMLDocument(xml: xmlString)
                        
                        if let content = xmlDoc.root["soap:Body"]["GetSearchNewsResponse"]["GetSearchNewsResult"]["diffgr:diffgram"]["NewDataSet"]["AllInitiatives"].all {
                            
                            for child in content {
                            
                                let article = Article()
                                article.imageTitle = child["ImageFullPath"].value
                                article.pKey = child["PKey"].value
                                article.header = child["Header"].value
                                article.date = child["CreatedDate"].value
                                result.append(article)
                                
                                counter = counter - 1;
                                
                                if(counter == 0) {
                                    break;
                                }
                            }
                            
                        
                        }
                        
                         completion(request!, result)
                        
                    } catch {
                        print("\(error)")
                    }
                }
                
        }
        
        
    }
    
    
    
    static func getNewsDetails(pKey : String , completion: @escaping (_ result: [Article]) -> Void) -> Void {
        
        
        var result = [Article]()
        let soapRequest = AEXMLDocument()
        let envelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema", "xmlns:soap12" : "http://www.w3.org/2003/05/soap-envelope"]
        let envelope = soapRequest.addChild(name: "soap12:Envelope", attributes: envelopeAttributes)
        let body = envelope.addChild(name: "soap12:Body")
        let child = body.addChild(name: "GetNewsDescriptionByID",  attributes: ["xmlns" : "http://tempuri.org/"])
        child.addChild(name: "strPKey", value: pKey )
        
        print(soapRequest.xml)
        
        let soapLenth = String(soapRequest.xml.characters.count)
        let theURL = NSURL(string: "http://d2495236.u68.gohsphere.com/GetNewsDescription.asmx")
        
        var mutableR = URLRequest(url: theURL! as URL)
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("text/html; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("utf-8", forHTTPHeaderField: "Accept-Charset")
        
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        Alamofire.request(mutableR)
            .responseString { response in
                if let xmlString = response.result.value {
                    
                    do {
                        let xmlDoc = try AEXMLDocument(xml: xmlString)
                        
                        
                        if let content = xmlDoc.root["soap:Body"]["GetNewsDescriptionByIDResponse"]["GetNewsDescriptionByIDResult"]["diffgr:diffgram"]["NewDataSet"]["AllInitiatives"].all {
                            
                            for child in content {
                                print(child["ImageName"].value)
                                
                                
                                let article = Article()
                                article.imageTitle = child["ImageName"].value
                                article.newsDesc = child["NewsDesc"].value
                                article.date = child["CreatedDate"].value
                                
                                result.append(article)
                                
                                
                                
                            }
                        }
                        
                        
                        completion(result)
                        
                    } catch {
                        print("\(error)")
                    }
                    
                }
                
        }
    }
    //MARK:Notification Service:---
    static func RegisterUserDevice(deviceToken : String ,deviceType : String , completion: @escaping (_ result: [Article]) -> Void) -> Void {
        
        var result = [Article]()
        let soapRequest = AEXMLDocument()
        let envelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema", "xmlns:soap12" : "http://www.w3.org/2003/05/soap-envelope"]
        let envelope = soapRequest.addChild(name: "soap12:Envelope", attributes: envelopeAttributes)
        let body = envelope.addChild(name: "soap12:Body")
        let child = body.addChild(name: "RegisterUserDevice",  attributes: ["xmlns" : "http://tempuri.org/"])
        
        child.addChild(name: "deviceToken", value: deviceToken )
        child.addChild(name: "deviceType", value: deviceType )
        
        print(soapRequest.xml)
        
        let soapLenth = String(soapRequest.xml.characters.count)
        let theURL = NSURL(string: "http://d2495236.u68.gohsphere.com/PushNotifications.asmx")
        
        var mutableR = URLRequest(url: theURL! as URL)
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("text/html; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("utf-8", forHTTPHeaderField: "Accept-Charset")
        
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        Alamofire.request(mutableR)
            .responseString { response in
                if let xmlString = response.result.value {
                    
                    print(xmlString)
                    
                    do {
                        let xmlDoc = try AEXMLDocument(xml: xmlString)
                        
                        let article = Article()
                        result.append(article)
                        
                        completion(result)
                        
                        
                        
                    } catch {
                        print("\(error)")
                    }
                    
                    
                    
                    
                }
                
        }
        
    }

    
    static func getVideos(completion: @escaping (_ result: [Article]) -> Void) -> Void {
        
        
        var result = [Article]()
        let soapRequest = AEXMLDocument()
        let envelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema", "xmlns:soap12" : "http://www.w3.org/2003/05/soap-envelope"]
        let envelope = soapRequest.addChild(name: "soap12:Envelope", attributes: envelopeAttributes)
        let body = envelope.addChild(name: "soap12:Body")
        let child = body.addChild(name: "GetVideos",  attributes: ["xmlns" : "http://tempuri.org/"])
        
        let soapLenth = String(soapRequest.xml.characters.count)
        let theURL = NSURL(string: "http://d2495236.u68.gohsphere.com/GetAllVideos.asmx")
        
        var mutableR = URLRequest(url: theURL! as URL)
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("text/html; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("utf-8", forHTTPHeaderField: "Accept-Charset")
        
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        Alamofire.request(mutableR)
            .responseString { response in
                if let xmlString = response.result.value {
                    
                    do {
                        let xmlDoc = try AEXMLDocument(xml: xmlString)
                        
                        
                        if let content = xmlDoc.root["soap:Body"]["GetVideosResponse"]["GetVideosResult"]["diffgr:diffgram"]["NewDataSet"]["AllInitiatives"].all {
                            
                            for child in content {
                                print(child["ImageName"].value)
                                
                                
                                let article = Article()
                                article.imageTitle = child["ImageName"].value
                                article.header = child["Heading"].value
                                article.website = child["VideoName"].value
                                
                                result.append(article)
                                
                                
                                
                            }

                        }
                        
                        
                        completion(result)
                    } catch {
                        print("\(error)")
                    }
                }
                
        }
    }
    
    
    static func getAlbums(completion: @escaping (_ result: [Album]) -> Void) -> Void {
        
        
        var result = [Album]()
        let soapRequest = AEXMLDocument()
        let envelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema", "xmlns:soap12" : "http://www.w3.org/2003/05/soap-envelope"]
        let envelope = soapRequest.addChild(name: "soap12:Envelope", attributes: envelopeAttributes)
        let body = envelope.addChild(name: "soap12:Body")
        body.addChild(name: "GetAlbums",  attributes: ["xmlns" : "http://tempuri.org/"])
        
        print(soapRequest.xml)
        
        let soapLenth = String(soapRequest.xml.characters.count)
        let theURL = NSURL(string: "http://d2495236.u68.gohsphere.com/GetImagesAlbums.asmx")
        
        var mutableR = URLRequest(url: theURL! as URL)
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("text/html; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("utf-8", forHTTPHeaderField: "Accept-Charset")
        
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        Alamofire.request(mutableR)
            .responseString { response in
                if let xmlString = response.result.value {
                    
                    do {
                        let xmlDoc = try AEXMLDocument(xml: xmlString)
                        
                        
                        if let content = xmlDoc.root["soap:Body"]["GetAlbumsResponse"]["GetAlbumsResult"]["diffgr:diffgram"]["NewDataSet"]["AllInitiatives"].all {
                            for child in content {
                                
                                let article = Album()
                                article.pKey = child["Pkey"].value
                                article.albumName = child["AlbumName"].value
                                article.year = Int(child["Year"].value != nil ? child["Year"].value! : String( moment().year))
                                article.imageName = child["ImageName"].value
                                article.imageFullPath = child["ImageFullPath"].value
                                
                                result.append(article)
                            }

                            
                        }
                        
                        
                        completion(result)
                        
                        
                    } catch {
                        print("\(error)")
                    }
                    
                }
                
        }
    }
    
    
    static func getAlbumDetails(pkey : String, completion: @escaping (_ result: [Album]) -> Void) -> Void {
        
        
        var result = [Album]()
        let soapRequest = AEXMLDocument()
        let envelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema", "xmlns:soap12" : "http://www.w3.org/2003/05/soap-envelope"]
        let envelope = soapRequest.addChild(name: "soap12:Envelope", attributes: envelopeAttributes)
        let body = envelope.addChild(name: "soap12:Body")
        let child = body.addChild(name: "GetImagesbyAlbum",  attributes: ["xmlns" : "http://tempuri.org/"])
        child.addChild(name: "strPKey", value: pkey )
        
        
        let soapLenth = String(soapRequest.xml.characters.count)
        let theURL = NSURL(string: "http://d2495236.u68.gohsphere.com/GetImagesByAlbum.asmx")
        
        var mutableR = URLRequest(url: theURL! as URL)
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("text/html; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("utf-8", forHTTPHeaderField: "Accept-Charset")
        
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        Alamofire.request(mutableR)
            .responseString { response in
                if let xmlString = response.result.value {
                    
                    do {
                        let xmlDoc = try AEXMLDocument(xml: xmlString)
                        
                        
                        if let content = xmlDoc.root["soap:Body"]["GetImagesbyAlbumResponse"]["GetImagesbyAlbumResult"]["diffgr:diffgram"]["NewDataSet"]["AllInitiatives"].all {
                            
                            for child in content {
                                
                                let article = Album()
                                article.imageFullPath = child["ImageFullPath"].value
                                result.append(article)
                            }

                            
                        }
                        
                        
                        completion(result)
                        
                    } catch {
                        print("\(error)")
                    }
                }
        }
    }
    
    
    
    
    static func getNewsCategories(completion: @escaping (_ result: [NewsCategory]) -> Void) -> Void {
        
        
        var result = [NewsCategory]()
        let soapRequest = AEXMLDocument()
        let envelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema", "xmlns:soap12" : "http://www.w3.org/2003/05/soap-envelope"]
        let envelope = soapRequest.addChild(name: "soap12:Envelope", attributes: envelopeAttributes)
        let body = envelope.addChild(name: "soap12:Body")
        body.addChild(name: "GetAllCategories",  attributes: ["xmlns" : "http://tempuri.org/"])
        
        let soapLenth = String(soapRequest.xml.characters.count)
        let theURL = NSURL(string: "http://d2495236.u68.gohsphere.com/ListAllNewsCategories.asmx")
        
        var mutableR = URLRequest(url: theURL! as URL)
        mutableR.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("text/html; charset=utf-8", forHTTPHeaderField: "Content-Type")
        mutableR.addValue("utf-8", forHTTPHeaderField: "Accept-Charset")
        
        mutableR.addValue(soapLenth, forHTTPHeaderField: "Content-Length")
        mutableR.httpMethod = "POST"
        mutableR.httpBody = soapRequest.xml.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        Alamofire.request(mutableR)
            .responseString { response in
                if let xmlString = response.result.value {
                    do {
                        let xmlDoc = try AEXMLDocument(xml: xmlString)
                        
                        
                        if let content = xmlDoc.root["soap:Body"]["GetAllCategoriesResponse"]["GetAllCategoriesResult"]["diffgr:diffgram"]["NewDataSet"]["AllInitiatives"].all {
                            
                            
                            for child in content {
                                let article = NewsCategory()
                                article.newsType = child["NewsType"].value
                                article.newsTypeId = child["NewsTypeID"].value
                                result.append(article)
                            }
                        }
                      
                        completion(result)
                        
                    } catch {
                        print("\(error)")
                    }
                }
        }
    }
}
