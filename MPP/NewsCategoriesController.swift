import Foundation
import XLPagerTabStrip
import SwiftEventBus

class NewsCategoriesController  : ButtonBarPagerTabStripViewController {
    
    
    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        secondCategories.first = false
        return [firstCategories, secondCategories]
    }
    
    
    var firstCategories = NewsTabsViewController()
    var secondCategories = NewsTabsViewController()
    
    
    override func viewDidLoad() {
        
        settings.style.buttonBarBackgroundColor = Colors.red
        settings.style.buttonBarHeight = 50
        
        settings.style.buttonBarItemBackgroundColor = Colors.red
        settings.style.buttonBarItemTitleColor = UIColor.white
        
        settings.style.buttonBarItemLeftRightMargin = 20
        
        settings.style.selectedBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemsShouldFillAvailableWidth = false
        
        settings.style.selectedBarHeight  = 3


        super.viewDidLoad()
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
        
        self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: size)

        
        
        SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
            let ev : TextSizeChange = result.object as! TextSizeChange
            self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: ev.textSize)
            
            self.buttonBarView.reloadData()
            
        }

//        
//        Api.getNewsCategories(completion: { (result) in
//            
//            var arrayOfNewsList =  [UIViewController]()
//
////            NewsTabsFragment.newInstance(Arrays.asList("wat","jew")),
////            NewsTabsFragment.newInstance(Arrays.asList("mn","ex","bn"))
//            
//            
//            var i = 0
//            for category in result {
//                
//                if("CompanyNews" != category.newsType!) {
//                    
//                    let controller = NewsViewController(pos: i, newsCategory : category.newsTypeId!, newsCategoryName: category.newsType!)
//                    arrayOfNewsList.append(controller)
//                
//                    i = i + 1
//                }
//            }
//            
////            self.firstCategories.arrayOfNewsList = arrayOfNewsList
////            self.secondCategories.arrayOfNewsList = arrayOfNewsList
//
//             
////            let innerViewController = NewsTabsViewController()
////            innerViewController.arrayOfNewsList = arrayOfNewsList
////            self.addChildViewController(innerViewController)
////            self.view.addSubview(innerViewController.view)
////            
////            innerViewController.didMove(toParentViewController: self)
//            
//        })
    }
}
