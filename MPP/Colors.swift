import Foundation
import UIKit
import HexColors

class Colors {
    
    static let red = UIColor("#cf0002");
    static let bg = UIColor("#f8f8f8");
    static let dark = UIColor("#222222")
    
    static let facebook = UIColor("4B7EBD");
    static let twitter = UIColor("0FD4FF")
    static let google = UIColor("FE5C4F")
    
    static let textSize = UIColor("C6D4DD")

    
    static let divider = UIColor("D1D1D1")
    
    static let textGalleryBg = UIColor("96000000")
    
    static let newsBg = UIColor("#F0F0F0")
}
