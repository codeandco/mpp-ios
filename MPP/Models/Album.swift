//
//  Album.swift
//  MPP
//
//  Created by Mateusz Orzoł on 24.02.2017.
//  Copyright © 2017 proexe. All rights reserved.
//

import Foundation

class Album {
    var pKey : String? = ""
    var albumName : String? = ""
    var year : Int?
    var imageName : String? = ""
    var imageFullPath : String? = ""
    
    var tag : Int?
}
