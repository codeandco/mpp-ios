//
//  Article.swift
//  MPP
//
//  Created by Mateusz Orzoł on 24.02.2017.
//  Copyright © 2017 proexe. All rights reserved.
//

import Foundation

class Article {
    var sortOrder:String? = ""
    var newsSDesc:String? = ""
    var imageTitle:String? = ""
    var newsDesc : String? = ""
    var pKey : String? = ""
    var header : String? = ""
    var website : String? = ""
    var date : String? = ""
    
}
