import UIKit
import Fabric
import Crashlytics

import Fabric
import TwitterKit
import FacebookCore
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self, Twitter.self])
        SDKSettings.appId = ("264883967294297")
        
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GIDSignIn.sharedInstance().delegate = self
        
        //and then
        
        
        if launchOptions != nil {
            if launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
                
                //Move To Notification Screen
                
                let dict = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
                
                if dict != nil {
                    let dict1 : NSDictionary = dict?["aps"] as! NSDictionary
                    Timer.scheduledTimer(timeInterval:0.1, target: self, selector: #selector(someSelector), userInfo: dict1, repeats: false)
                }
                
            }
        }
        
        // Register Push Notification:--
        registerForPushNotifications(application: application)
        
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: Colors.red ?? UIColor.red], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.black], for: .normal)
        
        return true
    }
    func someSelector(val : Timer?) {
       
         let dict1 : NSDictionary = val?.userInfo as! NSDictionary
        
        // let aps: NSDictionary = (val!.userInfo["aps"] as NSDictionary)!
        
           // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: val?.userInfo as! [AnyHashable : Any]?)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: dict1 as? [AnyHashable : Any])
    }
    
    func registerForPushNotifications(application: UIApplication) {
        
//        // iOS 10 support
//        if #available(iOS 10, *) {
//            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
//            application.registerForRemoteNotifications()
//        }
//            // iOS 9 support
//        else if #available(iOS 9, *) {
//            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
//            UIApplication.shared.registerForRemoteNotifications()
//        }
//            // iOS 8 support
//        else if #available(iOS 8, *) {
//            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
//            UIApplication.shared.registerForRemoteNotifications()
//        }
//            // iOS 7 support
//        else {
//            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
//        }
        
        
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                else{
                    //Do stuff if unsuccessful...
                }
            })
        }
            
        else{ //If user is not on iOS 10 use the old methods we've been using
            let notificationSettings = UIUserNotificationSettings(types: [UIUserNotificationType.badge, UIUserNotificationType.sound, UIUserNotificationType.alert], categories: nil)
            application.registerUserNotificationSettings(notificationSettings)
            
        }
        
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
        
        
   // }
    
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
//    {
//        completionHandler([.alert, .badge, .sound])
//    }
    func application(_ application: UIApplication, didReceive notification: UIRemoteNotificationType) {
        
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
       if let aps: NSDictionary = userInfo["aps"] as? NSDictionary {
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: aps as? [AnyHashable : Any])
//            
//            let state : UIApplicationState = application.applicationState
//            if (state == .inactive || state == .background) {
//                //Move To Notification Screen
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: aps as? [AnyHashable : Any])
//            } else {
//                //Move To Notification Screen
//                // App is in UIApplicationStateActive (running in foreground)
//                //  UIApplication.shared.applicationIconBadgeNumber = aps.value(forKey: "badge") as! Int
//                
//            }
        }
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        print("Ravi" , token)
        UserDefaults.standard.set(token, forKey: "deviceToken")
        Api.RegisterUserDevice(deviceToken: token, deviceType: "IOS", completion: { (result) in
            print(result)
            
        })
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error){
        print("i am not available in simulator \(error)")
    }
    
    /*
     func application(_ application: UIApplication,
     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
     return GIDSignIn.sharedInstance().handle(url,
     sourceApplication: sourceApplication,
     annotation: annotation)
     }
     // [END openurl]
     @available(iOS 9.0, *)
     func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
     return GIDSignIn.sharedInstance().handle(url,
     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
     }
     
     */
    func applicationWillEnterForeground(application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo:nil)
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        let sourceApplication =  options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        let annotation = options[UIApplicationOpenURLOptionsKey.annotation]
        
        let googleHandler = GIDSignIn.sharedInstance().handle(
            url,
            sourceApplication: sourceApplication,
            annotation: annotation )
        
        let facebookHandler = SDKApplicationDelegate.shared.application (
            app,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation )
        
        return googleHandler || facebookHandler
    }
    
    
    
    /*   func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
     return SDKApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
     }*/
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    
    
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
         UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
         UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
}

