import Foundation
import UIKit
import SnapKit
import Kingfisher
import MaterialComponents
import SwiftyGif

class GalleryDetailViewController : UIViewController, UIScrollViewDelegate {
    
    
    let scrollView = UIScrollView()
    
    
    static var pKey : String = ""
    static var year : String = "2017"
    static var textSize : CGFloat = UIFont.systemFontSize
    
    let currentPageLabel = UILabel()
    
    var totalCount : Int = 0
    static var cache =  [String:[Album]]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(scrollView)
        
        
//        activityIndicator.cycleColors = [Colors.red!]
//        activityIndicator.radius = 32
//        activityIndicator.strokeWidth = 2;
        
        
        
        
        let gifmanager = SwiftyGifManager(memoryLimit:20)
        let gif = UIImage(gifName: "dark_loader")
        let activityIndicator = UIImageView(gifImage: gif, manager: gifmanager)
        activityIndicator.frame =  CGRect(x: 0, y: 0, width: 15, height: 15)
        view.addSubview(activityIndicator)


        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.height.equalTo(80)
        }
        activityIndicator.startAnimating()
        
        
        
        
        let topBar = UIView();
        
        view.addSubview(topBar)
        topBar.backgroundColor = Colors.red
        
        topBar.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: 50)
        
        let pageLabel = UILabel()
        
        pageLabel.text = GalleryDetailViewController.year
        
        pageLabel.textColor = UIColor.white
        
        topBar.addSubview(pageLabel)
        
        
        pageLabel.font = UIFont.systemFont(ofSize: GalleryDetailViewController.textSize)
        currentPageLabel.font = UIFont.systemFont(ofSize: GalleryDetailViewController.textSize)

        
        topBar.addSubview(currentPageLabel)
        
        currentPageLabel.textColor = UIColor.white
        
        
        
        let backArrow = UIImageView()
        
        backArrow.image = UIImage(named: "ic_chevron_left_white")
        
        topBar.addSubview(backArrow)
        
        
        backArrow.snp.makeConstraints { (make) in
            make.centerY.equalTo(topBar)
            make.width.height.equalTo(35)
            make.left.equalTo(topBar).offset(10)
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        backArrow.isUserInteractionEnabled = true
        backArrow.addGestureRecognizer(tapGestureRecognizer)
        
        
        pageLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(topBar)
            make.left.equalTo(backArrow.snp.right).offset(15)
            make.right.equalTo(currentPageLabel.snp.left).offset(-10)
            make.width.lessThanOrEqualTo(100).priority(500)
        }
        
        
        currentPageLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(topBar)
            make.right.equalTo(topBar).offset(-10)
            
        }
       
    
//        self.automaticallyAdjustsScrollViewInsets = false;

//        self.scrollView.automaticallyAdjustsScrollViewInsets = false;
        self.scrollView.frame = CGRect(x: 0, y: 50, width:self.view.frame.width, height:self.view.frame.height)
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.scrollView.frame.height
        
        
        if let result =  GalleryDetailViewController.cache[GalleryDetailViewController.pKey] {
            
            self.totalCount = result.count
            self.currentPageLabel.text = "Photo \(1) of \(result.count)"
            self.currentPageLabel.sizeToFit()
            var i = CGFloat(0)
            for image in result {
                let imgOne = UIImageView(frame: CGRect(x: scrollViewWidth*i, y:0,width:scrollViewWidth, height:scrollViewHeight - 50))
                
                let url = URL(string:  image.imageFullPath!)
                
                
                imgOne.kf.setImage(with: url,
                                   placeholder: nil,
                                   options: [.transition(.fade(1))],
                                   progressBlock: nil,
                                   completionHandler: nil)
                
                imgOne.contentMode = .scaleAspectFit
                
                self.scrollView.addSubview(imgOne)
                i = i + 1
            }
            //            self.activityIndicator.stopAnimating()
            //activityIndicator.alpha = 0
            UIView.animate(withDuration: 0.3, animations: {
                activityIndicator.alpha = 0
            })
            
            self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * CGFloat(result.count), height:self.scrollView.frame.height)

        } else {
            Api.getAlbumDetails(pkey: GalleryDetailViewController.pKey, completion: { (result) in
                
                GalleryDetailViewController.cache.updateValue(result, forKey: GalleryDetailViewController.pKey)
                
                
                self.totalCount = result.count
                self.currentPageLabel.text = "Photo \(1) of \(result.count)"
                self.currentPageLabel.sizeToFit()
                var i = CGFloat(0)
                for image in result {
                    let imgOne = UIImageView(frame: CGRect(x: scrollViewWidth*i, y:0,width:scrollViewWidth, height:scrollViewHeight - 50))
                    
                    let url = URL(string:  image.imageFullPath!)
                    
                    
                    imgOne.kf.setImage(with: url,
                                       placeholder: nil,
                                       options: [.transition(.fade(0.5))],
                                       progressBlock: nil,
                                       completionHandler: nil)
                    
                    imgOne.contentMode = .scaleAspectFit
                    
                    self.scrollView.addSubview(imgOne)
                    i = i + 1
                }
                //            self.activityIndicator.stopAnimating()
                //activityIndicator.alpha = 0
                UIView.animate(withDuration: 0.3, animations: {
                    activityIndicator.alpha = 0
                })
                
                self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * CGFloat(result.count), height:self.scrollView.frame.height)
                
            })

        }
        
        
        self.scrollView.delegate = self
        self.scrollView.isPagingEnabled = true
        self.scrollView.backgroundColor = Colors.dark
        
        
        
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
//        willMove(toParentViewController: parent)
//        view.removeFromSuperview()
//        removeFromParentViewController()

        navigationController?.popViewController(animated: true)
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        
        let currentPage2 = Int(currentPage + 1)
        self.currentPageLabel.text = "Photo \(currentPage2) of \(totalCount)"
        self.currentPageLabel.sizeToFit()
    }
    
}

extension Collection {
    
    func chunk(withDistance distance: IndexDistance) -> [[SubSequence.Iterator.Element]] {
        var index = startIndex
        let iterator: AnyIterator<Array<SubSequence.Iterator.Element>> = AnyIterator {
            defer {
                index = self.index(index, offsetBy: distance, limitedBy: self.endIndex) ?? self.endIndex
            }
            
            let newIndex = self.index(index, offsetBy: distance, limitedBy: self.endIndex) ?? self.endIndex
            let range = index ..< newIndex
            return index != self.endIndex ? Array(self[range]) : nil
        }
        
        return Array(iterator)
    }
    
}
