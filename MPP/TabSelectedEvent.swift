import Foundation


class TabSelectedEvent {
    
    var position : Int = 0
    
    
    init(position : Int) {
        self.position = position
    }
}



class TextSizeChange {
    var textSize : CGFloat = 0.0
}
