import Foundation
import XLPagerTabStrip
import SwiftEventBus

class TermsControler : UIViewController, IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Terms")
    }

    
    
    var didSetupConstraints = false
    
    let scrollView  = UIScrollView()
    let contentView = UIView()
    
    let label: UILabel = {
        let label = UILabel()

        label.numberOfLines = 0
        label.lineBreakMode = .byClipping
        label.textColor = .black
        label.text = NSLocalizedString("MPP-ME has been constituted and operates as a responsible media organization under the governance of the current press laws of the UAE.\n\nWhilst \"Arabian Watches & Jewellery News Broadcast\", as a responsible, valuable service to the trade makes every effort to meticulously provide reliable, verifiable information reports and developments in the industry in the Middle East and adhere to fair press regulations, we are not accountable for misinterpretation or inadvertent, unintended, unforeseen errors that may emerge in the editing and dissemination process or for reasons beyond our control.\n\nNeither MPP-ME nor the Management can be held liable for monetary compensation or legal accountability arising from published information produced by our editorial team or derived from other business sources.", comment: "")
        
        
       
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        upImage.image = UIImage(named: "go_up")
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
        label.font = UIFont.systemFont(ofSize: size)

        view.backgroundColor = UIColor.white
        
        view.addSubview(scrollView)
        
        scrollView.addSubview(contentView)
        contentView.addSubview(label)
        contentView.addSubview(upImage)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        upImage.isUserInteractionEnabled = true
        upImage.addGestureRecognizer(tapGestureRecognizer)
        
        view.setNeedsUpdateConstraints()
        
        SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
            let ev : TextSizeChange = result.object as! TextSizeChange
            self.label.font = UIFont.systemFont(ofSize: (ev.textSize))
        }
        
        upImage.alpha = 0

    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        scrollView.scrollToTop()
    }
    
    override func updateViewConstraints() {
        
        if (!didSetupConstraints) {
            
            scrollView.snp.makeConstraints { make in
                make.edges.equalTo(view).inset(UIEdgeInsets.zero)
            }
            
            contentView.snp.makeConstraints { make in
                make.edges.equalTo(scrollView).inset(UIEdgeInsets.zero)
                make.width.equalTo(scrollView)
            }
            
            label.snp.makeConstraints { make in
                make.top.equalTo(contentView).inset(20)
                make.leading.equalTo(contentView).inset(20)
                make.trailing.equalTo(contentView).inset(20)
                make.bottom.equalTo(contentView).inset(100)
            }
            
            upImage.snp.makeConstraints { make in
                make.top.equalTo(label.snp.bottom).offset(20)
                make.centerX.equalTo(contentView)
                make.width.height.equalTo(60)
            }
            
            didSetupConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    let upImage = UIImageView()
}
