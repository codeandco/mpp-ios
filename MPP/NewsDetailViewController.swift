import Foundation
import UIKit
import MaterialComponents.MaterialFlexibleHeader
import SnapKit
import SwiftMoment
import DateToolsSwift
import ChameleonFramework
import SwiftyGif
import FacebookShare
import FacebookCore
import TwitterKit
import TwitterCore

class NewsDetailViewController : UIViewController, MDCFlexibleHeaderViewLayoutDelegate {
    
    
    @IBOutlet weak var newsContent: UITextView!
    
    @IBOutlet weak var scroll: UIScrollView!
    
    let appBar = MDCAppBar()
    public static var newsCategory : String? = ""
    public static var pKey : String? = ""
    public static var fullImage = false
    public static var fullImagePath = ""
    
    
    let aPlus = UILabel()
    let aMinus = UILabel()
    
    let share = UIImageView()
    let upImage = UIImageView()
    
    let topGradient = UIImageView()
    
    public static   var newsImage : URL?
    var minTextSize = UIFont.systemFontSize
    var maxTextSize  = UIFont.systemFontSize + 6
    
    
    var existingInteractivePopGestureRecognizerDelegate : UIGestureRecognizerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if navigationController?.interactivePopGestureRecognizer?.delegate != nil {
            existingInteractivePopGestureRecognizerDelegate = navigationController?.interactivePopGestureRecognizer?.delegate!
        }
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if existingInteractivePopGestureRecognizerDelegate != nil {
            navigationController?.interactivePopGestureRecognizer?.delegate = existingInteractivePopGestureRecognizerDelegate!
        }
    }
    
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        appBar.headerViewController.headerView.maximumHeight = 200;
        self.addChildViewController(appBar.headerViewController)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    let imageView = UIImageView()
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        scroll.scrollToTop()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        upImage.image = UIImage(named: "go_up")
        
        upImage.alpha = 0
        
        upImage.isUserInteractionEnabled = true
        upImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:))))
        
        
        self.scroll.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        self.scroll.addSubview(upImage)
        
        
        upImage.snp.makeConstraints { make in
            make.top.equalTo(newsContent.snp.bottom).offset(20)
            make.centerX.equalTo(scroll)
            make.width.height.equalTo(60)
        }
        
        
        let headerView = appBar.headerViewController.headerView
        
        
        let first = UIBarButtonItem(title: "A-",
                                    style: .plain,
                                    target: self,
                                    action: #selector(minusTapped(tapGestureRecognizer:)))
        
        let second = UIBarButtonItem(title: "A+",
                                     style: .plain,
                                     target: self,
                                     action: #selector(plusTapped(tapGestureRecognizer:)))
        
        
        
        
        let button = UIButton.init(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "share"), for: UIControlState.normal)
        //add function for button
        button.addTarget(self, action: #selector(shareTapped(button:)), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 53, height: 51)
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
        let barButton = UIBarButtonItem(customView: button)
        
        
        navigationItem.rightBarButtonItems = [barButton, second, first ]
        
        appBar.headerViewController.layoutDelegate  = self
        
        imageView.frame = headerView.bounds
        imageView.backgroundColor = Colors.dark
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        headerView.insertSubview(imageView, at: 0)
        
        
        let timeLabel = UILabel();
        timeLabel.textColor = UIColor.white
        headerView.insertSubview(timeLabel, at: 1)
        
        headerView.insertSubview(topGradient, at: 1)
        
        
        
        aMinus.text = "A-"
        aPlus.text = "A+"
        
        aPlus.textColor = UIColor.white
        aMinus.textColor = UIColor.white
        share.image = UIImage(named: "share")
        
        topGradient.snp.makeConstraints { (make) in
            make.width.height.equalTo(imageView)
        }
        
        
        
        timeLabel.snp.makeConstraints { (make) in
            make.right.equalTo(headerView).offset(-15)
            make.bottom.equalTo(headerView).offset(-15)
        }
        
        
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        
        headerView.tintColor = UIColor.white
        appBar.navigationBar.titleTextAttributes =
            [ NSForegroundColorAttributeName: UIColor.white ]
        
        headerView.maximumHeight = 240
        headerView.backgroundColor = Colors.dark
        
        
        appBar.headerViewController.headerView.trackingScrollView = self.scroll
        self.scroll.delegate = appBar.headerViewController
        appBar.addSubviewsToParent()
        
        
        let gifmanager = SwiftyGifManager(memoryLimit:20)
        let gif = UIImage(gifName: "loader")
        let activityIndicator = UIImageView(gifImage: gif, manager: gifmanager)
        activityIndicator.frame =  CGRect(x: 0, y: 0, width: 15, height: 15)
        view.addSubview(activityIndicator)
        
        
        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.height.equalTo(80)
        }
        activityIndicator.startAnimating()
        
        
        
        
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
        timeLabel.font = UIFont.systemFont(ofSize: size)
        
        
        
        
        Api.getNewsDetails(pKey: NewsDetailViewController.pKey!,  completion: { (result) in
            print(result)
            
            if(result.isEmpty) {
                
            }
            
            if(NewsDetailViewController.fullImage) {
                
                
                var   url = URL(string: NewsDetailViewController.fullImagePath)
                
                
                self.imageView.kf.setImage(with: url,
                                           placeholder: nil,
                                           options: [.transition(.fade(1))],
                                           progressBlock: nil,
                                           completionHandler: nil)
                
                
            } else {
                if let categorySafe = NewsDetailViewController.newsCategory {
                    
                    if let imageSafe = result[0].imageTitle {
                        
                        var url = URL(string: String(format: Api.IMAGE_URL, categorySafe) + imageSafe)
                        
                        
                        self.imageView.kf.setImage(with: url,
                                                   placeholder: nil,
                                                   options: [.transition(.fade(1))],
                                                   progressBlock: nil,
                                                   completionHandler: nil)
                    }
                    
                } else {
                    
                    if let newsImageUrl = NewsDetailViewController.newsImage {
                        var url = NewsDetailViewController.newsImage
                        
                        
                        self.imageView.kf.setImage(with: url,
                                                   placeholder: nil,
                                                   options: [.transition(.fade(1))],
                                                   progressBlock: nil,
                                                   completionHandler: nil)
                    }
                    
                }
                
            }
            
            
            
            
            
            
            self.newsContent.attributedText = result[0].newsDesc?.utf8Data?.attributedString
            self.newsContent.isScrollEnabled = false
            
            
            timeLabel.text = result[0].date
            
            
            if (result[0].date != nil) {
                let yesterday = moment(result[0].date!)
                let fixed = yesterday?.add(2.hours)
                let d = fixed?.date
                
                if let dd = d  {
                    
                    if (dd.daysAgo == 0) {
                        
                        if(dd.hoursAgo == 0) {
                            if(dd.minutesAgo == 0) {
                                timeLabel.text = "just now"
                            } else {
                                timeLabel.text = "\(dd.minutesAgo) minutes ago"
                            }
                            
                        } else {
                            timeLabel.text = "\(dd.hoursAgo) hours ago"
                        }
                        
                    } else {
                        
                        timeLabel.text = "\(dd.daysAgo) days ago"
                    }
                    
                }
                
            }
            
            self.newsContent.setHTMLFromString(htmlText: result[0].newsDesc!)
            
            
            let textSize = UserDefaults.standard.value(forKey: "textSize")
            let size = UIFont.systemFontSize + CGFloat(textSize != nil ? textSize as! Float : 0)
            self.newsContent.font = UIFont.systemFont(ofSize: size)
            
            
            UIView.animate(withDuration: 0.3, animations: {
                activityIndicator.alpha = 0
            })
            self.upImage.alpha = 1
            
        })
    }
    
    func shareTapped(button: UIButton)
    {
        
        
        print("tapped")
        let myActionSheet = UIAlertController(title: "Color", message: "What color would you like?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        myActionSheet.title = nil
        myActionSheet.message = nil
        
        
        let shareLink = "http://www.mpp-me.com/";
        
        // blue action button
        let blueAction = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default) { (action) in
            print("Blue action button tapped")
            
            
            
            let content = LinkShareContent(url: URL(string: shareLink)!)
            
            
            let shareDialog = ShareDialog(content: content)
            shareDialog.mode = .automatic
            shareDialog.failsOnInvalidData = true
            shareDialog.completion = { result in
                // Handle share results
            }
            
            do {
                
                try shareDialog.show()
                
            }catch {
                
            }
            
        }
        
        // red action button
        let redAction = UIAlertAction(title: "Twitter", style: UIAlertActionStyle.default) { (action) in
            print("Red action button tapped")
            
            let composer = TWTRComposer()
            
            composer.setText("")
            //            composer.setImage(UIImage(named: "fabric"))
            
            // Called from a UIViewController
            composer.show(from: self) { result in
                if (result == TWTRComposerResult.cancelled) {
                    print("Tweet composition cancelled")
                }
                else {
                    print("Sending tweet!")
                }
            }
        }
        
        // yellow action button
        let yellowAction = UIAlertAction(title: "Google +", style: UIAlertActionStyle.default) { (action) in
            print("Yellow action button tapped")
            
            // Construct the Google+ share URL
            var urlComponents = URL(string:"https://plus.google.com/share?url=" + shareLink)
            UIApplication.shared.openURL(urlComponents!)
        }
        
        // cancel action button
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (action) in
            print("Cancel action button tapped")
        }
        
        // add action buttons to action sheet
        myActionSheet.addAction(blueAction)
        myActionSheet.addAction(redAction)
        myActionSheet.addAction(yellowAction)
        myActionSheet.addAction(cancelAction)
        
        // support iPads (popover view)
        myActionSheet.popoverPresentationController?.sourceView = button
        myActionSheet.popoverPresentationController?.sourceRect = button.bounds
        
        // present the action sheet
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func plusTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        print("tapped")
        
        if(self.newsContent.font!.pointSize == maxTextSize) {
            return
        }
        
        self.newsContent.font = UIFont.systemFont(ofSize: self.newsContent.font!.pointSize + 1)
    }
    
    func minusTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if(self.newsContent.font!.pointSize == minTextSize) {
            return
        }
        
        print("tapped")
        self.newsContent.font = UIFont.systemFont(ofSize: self.newsContent.font!.pointSize - 1)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        let colors:[UIColor] = [
            UIColor.black,
            UIColor.clear ,
            UIColor.clear ,
            
            UIColor.black
        ]
        
        topGradient.backgroundColor = GradientColor(.topToBottom, frame: topGradient.frame, colors: colors)
        topGradient.alpha = 0.5
    }
    
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        print("update view constraints")
    }
    
    func flexibleHeaderViewController(_ flexibleHeaderViewController: MDCFlexibleHeaderViewController,
                                      flexibleHeaderViewFrameDidChange flexibleHeaderView: MDCFlexibleHeaderView) {
        
        
        if(flexibleHeaderView.frame.height >= 240) {
            imageView.alpha = 1
        } else {
            imageView.alpha = 1 - (76  / flexibleHeaderView.frame.height) ;
        }
        
        print(76 / flexibleHeaderView.frame.height )
        
        
        //
        //        topGradient.snp.remakeConstraints { (make) in
        //
        //            make.width.equalTo(imageView)
        //            make.height.equalTo(imageView).dividedBy(2)
        //        }
        //
        let colors:[UIColor] = [
            UIColor.black,
            UIColor.clear ,
            UIColor.clear ,
            
            UIColor.black
        ]
        
        topGradient.backgroundColor = GradientColor(.topToBottom, frame: topGradient.frame, colors: colors)
        topGradient.alpha = 0.5
        
        
        
        //
        //        bottomGradient.snp.remakeConstraints  { (make) in
        //
        //            make.width.equalTo(imageView)
        //            make.height.equalTo(imageView).dividedBy(2)
        //            make.bottom.equalTo(imageView)
        //        }
        //
        //        let colors:[UIColor] = [
        //            UIColor.black,
        //            UIColor.clear
        //        ]
        //        bottomGradient.backgroundColor = GradientColor(.topToBottom, frame: bottomGradient.frame, colors: colors)
        //
        //        bottomGradient.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(M_PI)) / 180.0)
        //        topGradient.backgroundColor = GradientColor(.topToBottom, frame: CGRect(x: topGradient.frame.origin.x, y: topGradient.frame.origin.y, width: topGradient.frame.size.width, height: topGradient.frame.size.height + 20), colors: colors)
        //
        //
        //        topGradient.alpha = 0.5
        //        bottomGradient.alpha = 0.5
        
        
        
        if (76 / flexibleHeaderView.frame.height > 0.6) {
            navigationItem.rightBarButtonItems = []
        } else {
            if(navigationItem.rightBarButtonItems == nil || navigationItem.rightBarButtonItems?.count == 0) {
                
                
                let first = UIBarButtonItem(title: "A-",
                                            style: .plain,
                                            target: self,
                                            action: #selector(minusTapped(tapGestureRecognizer:)))
                
                let second = UIBarButtonItem(title: "A+",
                                             style: .plain,
                                             target: self,
                                             action: #selector(plusTapped(tapGestureRecognizer:)))
                
                let button = UIButton.init(type: .custom)
                //set image for button
                button.setImage(UIImage(named: "share"), for: UIControlState.normal)
                //add function for button
                button.addTarget(self, action: #selector(shareTapped(button:)), for: UIControlEvents.touchUpInside)
                //set frame
                button.frame = CGRect(x: 0, y: 0, width: 53, height: 51)
                button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                
                let barButton = UIBarButtonItem(customView: button)
                
                
                navigationItem.rightBarButtonItems = [barButton, second, first ]
            }
        }
    }
    
}
extension String {
    var length: Int {
        return self.characters.count
    }
}

extension UITextView {
    func setHTMLFromString(htmlText: String) {
        let modifiedFont = NSString(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(self.font!.pointSize)\">%@</span>" as NSString, htmlText) as String
        
        
        //process collection values
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        
        self.attributedText = attrStr
    }
}

extension Data {
    var attributedString: NSAttributedString? {
        do {
            let ret = NSMutableAttributedString(attributedString: try NSAttributedString(data: self, options:[NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil))
            
            return ret
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
}
extension String {
    var utf8Data: Data? {
        return data(using: .utf8)
    }
}
