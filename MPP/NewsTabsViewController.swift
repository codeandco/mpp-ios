import Foundation
import UIKit
import Kingfisher
import MaterialComponents
import SnapKit
import XLPagerTabStrip
import SwiftEventBus

class NewsTabsViewController : ButtonBarPagerTabStripViewController , IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: first ? "Novelties" : "News")
    }

    var first = true
    var arrayOfNewsList =  [UIViewController]()
    
    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
   
   
        if(first) {
            arrayOfNewsList.append( NewsViewController(pos: 0, newsCategory : "wat", newsCategoryName: "Watches"))
            arrayOfNewsList.append( NewsViewController(pos: 1, newsCategory : "jew", newsCategoryName: "Jewellery"))
        } else {
            //            NewsTabsFragment.newInstance(Arrays.asList("mn","ex","bn"))

            arrayOfNewsList.append( NewsViewController(pos: 0, newsCategory : "mn", newsCategoryName: "Market News"))
            arrayOfNewsList.append( NewsViewController(pos: 1, newsCategory : "ex", newsCategoryName: "Exhibition & Events"))
            arrayOfNewsList.append( NewsViewController(pos: 2, newsCategory : "bn", newsCategoryName: "Interviews"))

        }
        
        return arrayOfNewsList
    }

    override func viewDidLoad() {
        
        settings.style.buttonBarBackgroundColor = Colors.red
        settings.style.buttonBarHeight = 55
        
        settings.style.buttonBarItemBackgroundColor = Colors.red
        settings.style.buttonBarItemTitleColor = UIColor.white
        
        settings.style.buttonBarItemLeftRightMargin = 20
    
        settings.style.selectedBarHeight  = 3
        settings.style.buttonBarItemTitleColor = UIColor.black

        settings.style.buttonBarBackgroundColor = Colors.newsBg
        settings.style.buttonBarHeight = 40
        
        settings.style.buttonBarItemBackgroundColor = Colors.newsBg

        
        settings.style.selectedBarBackgroundColor = Colors.red!
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
    
        
        super.viewDidLoad()
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
 
        self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: size)

        
        if(!first) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.moveToViewController(at: 1, animated: false)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.moveToViewController(at: 0, animated: false)
            }
        }
        }

        
        
        SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
            let ev : TextSizeChange = result.object as! TextSizeChange
            self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: ev.textSize)
            
            self.buttonBarView.reloadData()
            
        }
        
        

    
    }
    
}
