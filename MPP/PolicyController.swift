import Foundation
import SnapKit
import XLPagerTabStrip
import SwiftEventBus

class PolicyController : UIViewController, IndicatorInfoProvider {
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Privacy Policy")
    }

    
    var didSetupConstraints = false
    
    let scrollView  = UIScrollView()
    let contentView = UIView()
    
    let label: UILabel = {
        let label = UILabel()

        label.numberOfLines = 0
        label.lineBreakMode = .byClipping
        label.textColor = .black
        label.text = NSLocalizedString("The \"Arabian Watches & Jewellery News Broadcast\" is a service provided to senior industry professionals and business executives, watch and jewellery manufacturers, retailers, distributors, investors, media and Public Relations consultants globally. No portion of the text in this newsletter may be replicated, copied, transferred, redirected, redistributed, recompiled or published in any form without the express consent of the owners of this site. \n\nPermission\n\nIf you wish to use this Broadcast in any manner other than for information or for your personal reading enjoyment, please send an e-mail to info@mpp-me.com.", comment: "")
        
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
        label.font = UIFont.systemFont(ofSize: size)

        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        upImage.image = UIImage(named: "go_up")
         
        
        view.backgroundColor = UIColor.white
        
        view.addSubview(scrollView)
        

        scrollView.addSubview(contentView)
        contentView.addSubview(label)
        contentView.addSubview(upImage)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        upImage.isUserInteractionEnabled = true
        upImage.addGestureRecognizer(tapGestureRecognizer)

        view.setNeedsUpdateConstraints()
        
        SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
            let ev : TextSizeChange = result.object as! TextSizeChange
            self.label.font = UIFont.systemFont(ofSize: (ev.textSize))
        }
        
        
        upImage.alpha = 0
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        scrollView.scrollToTop()
    }
    
    override func updateViewConstraints() {
        
        if (!didSetupConstraints) {
            
            scrollView.snp.makeConstraints { make in
                make.edges.equalTo(view).inset(UIEdgeInsets.zero)
            }
            
            contentView.snp.makeConstraints { make in
                make.edges.equalTo(scrollView).inset(UIEdgeInsets.zero)
                make.width.equalTo(scrollView)
            }
            
            label.snp.makeConstraints { make in
                make.top.equalTo(contentView).inset(20)
                make.leading.equalTo(contentView).inset(20)
                make.trailing.equalTo(contentView).inset(20)
                make.bottom.equalTo(contentView).inset(100)
            }
            
            upImage.snp.makeConstraints { make in
                make.top.equalTo(label.snp.bottom).offset(20)
                make.centerX.equalTo(contentView)
                make.width.height.equalTo(60)
            }
            
            didSetupConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    let upImage = UIImageView()
}


extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
}
