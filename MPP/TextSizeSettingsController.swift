import Foundation
import UIKit
import SnapKit
import XLPagerTabStrip
import SwiftEventBus


class TextSizeSettingsController : UIViewController, IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Text size")
    }
    
    
    let bottomLabel = UILabel()
    
    let bottomLabelWrapper = UIView()

    let useSystemSize = UILabel()
    
    let slider = UISlider()
    
    let useSystemSizeSwitch = UISwitch()
    
    let explanation = UILabel()
    
    let divider1 = UIView()
    
    let divider2 = UIView()
    
    let explanationWrapper = UIView();
    
    let aSmall = UILabel()
    
    let aBig = UILabel();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(useSystemSize)
        self.view.addSubview(slider)
        self.view.addSubview(useSystemSizeSwitch)
        self.view.addSubview(divider1)
        self.view.addSubview(divider2)
        self.view.addSubview(explanationWrapper)
        self.view.addSubview(aSmall)
        self.view.addSubview(aBig)
        self.view.addSubview(bottomLabelWrapper)

        
        bottomLabel.numberOfLines = 5
        
        explanationWrapper.addSubview(explanation)
        bottomLabelWrapper.addSubview(bottomLabel)

        
        divider1.backgroundColor = Colors.divider
        divider2.backgroundColor = Colors.divider

        
        useSystemSize.text = "Use system text size"
        explanation.text = "Move the slider to the left or\nright to adjust text size as per\nyour reading comfort"
        explanation.numberOfLines = 5
        explanation.sizeToFit()
        
        slider.minimumValue = 0
        slider.maximumValue = 30
        
        useSystemSizeSwitch.onTintColor = Colors.red
        
        useSystemSize.font = UIFont.boldSystemFont(ofSize: useSystemSize.font.pointSize + 1)
        
        aSmall.text = "A"
        aBig.text = "A"
        aBig.font = UIFont.boldSystemFont(ofSize: useSystemSize.font.pointSize + 5)

        
        
        self.view.backgroundColor = UIColor.white
        
        bottomLabel.text = "Switch off to set text in the app"
        bottomLabelWrapper.backgroundColor = Colors.textSize
        
        
        bottomLabel.sizeToFit()
        
        bottomLabelWrapper.snp.makeConstraints { make in
            make.width.equalTo(self.view)
            make.bottom.equalTo(self.view)
            make.top.equalTo(self.view.snp.centerY).dividedBy(0.95)
        }
        
        bottomLabel.snp.makeConstraints { make in
            make.top.left.equalTo(bottomLabelWrapper).offset(10)
            make.right.lessThanOrEqualTo(bottomLabelWrapper).offset(-10)

        }

        
        
        slider.snp.makeConstraints { make in
            make.bottom.equalTo(bottomLabel.snp.top).offset(-20)
            make.left.equalTo(self.view).offset(40)
            make.right.equalTo(self.view).offset(-40)
        }
        
        aSmall.snp.makeConstraints { make in
            make.centerY.equalTo(slider)
            make.left.equalTo(self.view).offset(10)
          
        }

        aBig.snp.makeConstraints { make in
           make.centerY.equalTo(slider)
            make.right.equalTo(self.view).offset(-10)

        }

        
        
        useSystemSizeSwitch.snp.makeConstraints { make in
              make.top.right.equalTo(self.view).inset(10)
        }
        
        useSystemSize.snp.makeConstraints { make in
            make.left.equalTo(self.view).offset(10)
            make.centerY.equalTo(useSystemSizeSwitch)
        }
        
        divider1.snp.makeConstraints { make in
            make.top.equalTo(useSystemSizeSwitch.snp.bottom).offset(10)
            make.left.equalTo(self.view).offset(5)
            make.right.equalTo(self.view).offset(-5)
            make.height.equalTo(1)
            
        }
        
        divider2.snp.makeConstraints { make in
            make.bottom.equalTo(slider.snp.top).offset(-10)
            make.left.equalTo(self.view).offset(5)
            make.right.equalTo(self.view).offset(-5)
            make.height.equalTo(1)

        }
        

        explanationWrapper.snp.makeConstraints { make in
            make.top.equalTo(divider1.snp.bottom)
            make.bottom.equalTo(divider2.snp.top)
            make.left.equalTo(self.view).offset(5)
//            make.right.equalTo(self.view).offset(-5)
            make.right.lessThanOrEqualTo(self.view).offset(-10)

        }
        
        explanation.snp.makeConstraints { make in
            make.centerY.equalTo(explanationWrapper)
            make.left.equalTo(explanationWrapper).offset(5)

            
        }
       
        
        
        slider.addTarget(self, action:#selector(self.sliderAction), for: .valueChanged)
        
        let textSize = UserDefaults.standard.value(forKey: "textSize")
        slider.setValue(textSize as? Float != nil ? (textSize as! Float) * 4 : 0, animated: false)
        let size = UIFont.systemFontSize + CGFloat(slider.value)
        bottomLabel.font = UIFont.systemFont(ofSize: size)
      
        
        let result = UserDefaults.standard.value(forKey: "useSystemSize") as? Bool
        useSystemSizeSwitch.setOn(result == nil ? false : result!, animated: false)
        
        useSystemSizeSwitch.addTarget(self, action:#selector(self.switchChanged), for: .valueChanged)
        
        

        
    }
    
    
    func switchChanged() {
        let value = useSystemSizeSwitch.isOn
        UserDefaults.standard.set(value, forKey: "useSystemSize")
        
        if(value) {
            slider.setValue(0, animated: false)
            bottomLabel.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
            useSystemSize.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
            explanation.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)

            bottomLabel.sizeToFit()
            explanation.sizeToFit()

            UserDefaults.standard.set(0, forKey: "textSize")
        }
        
 
        let event = TextSizeChange()
        event.textSize = UIFont.systemFontSize + CGFloat(slider.value / 4)
        
        
        SwiftEventBus.post("textSizeChange", sender: event)

    }
    
    func sliderAction() {
        let size = UIFont.systemFontSize + CGFloat(slider.value / 4)
        bottomLabel.font = UIFont.systemFont(ofSize: size)
        useSystemSize.font = UIFont.boldSystemFont(ofSize: size)
        explanation.font = UIFont.systemFont(ofSize: size)

        UserDefaults.standard.set(slider.value / 4, forKey: "textSize")
        bottomLabel.sizeToFit()
        explanation.sizeToFit()

        
        if(slider.value == 0) {
            useSystemSizeSwitch.setOn(true, animated: true)
            let value = useSystemSizeSwitch.isOn
            UserDefaults.standard.set(value, forKey: "useSystemSize")
        } else if (useSystemSizeSwitch.isOn) {
            useSystemSizeSwitch.setOn(false, animated: true)
            let value = useSystemSizeSwitch.isOn
            UserDefaults.standard.set(value, forKey: "useSystemSize")
        }
        
      
        let event = TextSizeChange()
        event.textSize = UIFont.systemFontSize + CGFloat(slider.value / 4)
        
        SwiftEventBus.post("textSizeChange", sender: event)
    }
}
