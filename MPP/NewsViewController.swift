import Foundation
import UIKit
import MaterialComponents
import XLPagerTabStrip
import SwiftyGif
import SwiftEventBus

class NewsViewController : UITableViewController , IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: newsCategoryName)
    }
    
    convenience init() {
        self.init(pos :0 , newsCategory: "", newsCategoryName : "")
    }
    
    var textSize = UIFont.systemFontSize

    
    init(pos: Int, newsCategory: String, newsCategoryName: String) {
        self.newsCategory = newsCategory
        self.pos = pos;
        self.newsCategoryName = newsCategoryName
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    var pos : Int = 0
    
    public var newsCategory : String = "bn"
    public var newsCategoryName : String = ""
    
    let UserCell = "ReceiptCell"
    let UserCell2 = "ReceiptCell2"
    
    var currentUsers: [Article] = [Article]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.bg

        
       
         NotificationCenter.default.addObserver(self, selector: #selector(self.navigateToDetails), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
        
        tableView.separatorStyle = .none
        tableView.register(NewsCellHeader.self, forCellReuseIdentifier: UserCell2)
        tableView.register(NewsCell.self, forCellReuseIdentifier: UserCell)
        
        self.view.tag = pos
        self.view.backgroundColor = Colors.bg
        
        
        
        let gifmanager = SwiftyGifManager(memoryLimit:20)
        let gif = UIImage(gifName: "loader")
        let activityIndicator = UIImageView(gifImage: gif, manager: gifmanager)
        activityIndicator.frame =  CGRect(x: 0, y: 0, width: 15, height: 15)
        view.addSubview(activityIndicator)
        
        
        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.height.equalTo(80)

        }
        activityIndicator.startAnimating()
        
        
        
        Api.getNews(newsCategory: self.newsCategory,  completion: { (result) in
            print(result)
            self.currentUsers = result
//            self.tableView.reloadData()
//            activityIndicator.stopAnimating()
            
            
            let textSize = UserDefaults.standard.value(forKey: "textSize")
            let size = UIFont.systemFontSize + CGFloat(textSize as? Float != nil ? textSize as! Float : 0)
            self.textSize = size
            
            
            self.tableView.reloadData()
            
            UIView.animate(withDuration: 0.3, animations: {
                activityIndicator.alpha = 0
            })
            
            
            SwiftEventBus.onMainThread(self, name: "textSizeChange") { result in
                let ev : TextSizeChange = result.object as! TextSizeChange
                self.textSize = ev.textSize
                self.tableView.reloadData()
                
                
                
            }

            
//            UIView.animate(withDuration: 0.3, animations: {
//                activityIndicator.alpha = 0
//            })
            
        })
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//       // NotificationCenter.default.addObserver(self, selector: #selector(self.catchIt), name: NSNotification.Name(rawValue: "myNotif"), object: nil)
//        
//        //Notification Method:---
//        NotificationCenter.default.addObserver(self, selector: #selector(self.navigateToDetails), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
//        
//    }
//    override func viewWillDisappear(_ animated: Bool) {
//        NotificationCenter.default.removeObserver(self)
//    }
   
    func navigateToDetails(_ notification: NSNotification) {
        
        let notificationData : NSDictionary = (notification.userInfo! as? NSDictionary)!
//        var alertView = UIAlertView()
//        alertView = UIAlertView(title: "Oops!", message: "\(notificationData)", delegate: nil, cancelButtonTitle: "OK");
//        alertView.show()
        NewsDetailViewController.pKey = notificationData.value(forKey:"pkey") as! String?
        
        NewsDetailViewController.newsCategory = notificationData.value(forKey:"NewsTypeID") as! String?
        navigationController?.pushViewController(NewsDetailViewController(nibName: "NewsDetailView", bundle: nil), animated: true)
        
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentUsers.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let screenSize = UIScreen.main.bounds
        
        if(indexPath.row == 0) {
            return screenSize.height / 2.5 + 60
            
        }
        
        return screenSize.height / 3.5 - 40 //todo
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.imageView?.kf.cancelDownloadTask()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Colors.bg
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let imageTitle = self.currentUsers[indexPath.row].imageTitle ?? ""
        let url = URL(string: String(format: Api.IMAGE_URL, self.newsCategory) + imageTitle)
        
        
        
        NewsDetailViewController.pKey = self.currentUsers[indexPath.row].pKey
        NewsDetailViewController.newsCategory = self.newsCategory
        NewsDetailViewController.fullImage = false
        NewsDetailViewController.newsImage = url

        navigationController?.pushViewController(NewsDetailViewController(nibName: "NewsDetailView", bundle: nil), animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if(indexPath.row == 0) {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: UserCell2)
                ?? UITableViewCell(style: .subtitle, reuseIdentifier: UserCell2) as! NewsCellHeader
            
            
            let onlineUserEmail = currentUsers[indexPath.row]
            
            
            (cell as! NewsCellHeader).eventName.text =  onlineUserEmail.header
            (cell as! NewsCellHeader).eventName.font = UIFont.systemFont(ofSize: textSize)
//            (cell as! NewsCellHeader).eventName.sizeToFit()

            if(  (cell as! NewsCellHeader).eventName.font?.pointSize != textSize) {
                (cell as! NewsCellHeader).eventName.sizeToFit()
            }
            

            
            
            if(onlineUserEmail.imageTitle != nil) {
                let url = URL(string: String(format: Api.IMAGE_URL, self.newsCategory) + onlineUserEmail.imageTitle!)
                
                (cell as! NewsCellHeader).eventImage.kf.setImage(with: url,
                                                                 placeholder: nil,
                                                                 options: [.transition(.fade(0.5))],
                                                                 progressBlock: nil,
                                                                 completionHandler: nil)
                
            }
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: UserCell)
                ?? UITableViewCell(style: .subtitle, reuseIdentifier: UserCell) as! NewsCell
            
            
            let onlineUserEmail = currentUsers[indexPath.row]
            
            
            (cell as! NewsCell).eventName.text = onlineUserEmail.header
            (cell as! NewsCell).eventName.font = UIFont.systemFont(ofSize: textSize)
//            (cell as! NewsCell).eventName.sizeToFit()

            if(  (cell as! NewsCell).eventName.font?.pointSize != textSize) {
                (cell as! NewsCell).eventName.sizeToFit()
            }

            
            if(onlineUserEmail.imageTitle != nil) {
                let url = URL(string: String(format: Api.IMAGE_URL, self.newsCategory) + onlineUserEmail.imageTitle!)
                
                
                (cell as! NewsCell).eventImage.kf.setImage(with: url,
                                                           placeholder: nil,
                                                           options: [.transition(.fade(0.5))],
                                                           progressBlock: nil,
                                                           completionHandler: nil)
                
            }
            return cell
        }
    }
}
