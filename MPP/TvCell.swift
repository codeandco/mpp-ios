import Foundation
import Foundation
import MaterialCard
import UIKit
import SnapKit


class TvCell : UITableViewCell {
    
    
    public var eventName: UITextView = UITextView()
    var eventImage : UIImageView = UIImageView()
    var card : MaterialCard = MaterialCard()
    let playImage = UIImageView()
    
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {

            var frame = newFrame
            frame.origin.x += 4
            frame.size.width -= 5
            super.frame = frame
            
//            let inset: CGFloat = 15
//            var frame = newFrame
//            frame.origin.x -= inset
//            frame.size.width += 2 * inset
//            super.frame = frame
        }
    }
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        card.backgroundColor = UIColor.white
//        card.shadowOffsetHeight = 0
        
        card.backgroundColor = UIColor.white
        card.shadowOffsetHeight = 2
        card.shadowOffsetWidth =  Int(card.frame.size.width)
        card.shadowColor = UIColor.black.withAlphaComponent(0.2)
        card.shadowOpacity = 0.25

        
        
//        card.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
//        card.layer.shadowOffset = CGSize(width: card.frame.size.width, height: 4)
//        card.layer.shadowOpacity = 0.2;
//        card.layer.shadowRadius = 1.0;
//        
        selectionStyle = .none
        
        self.contentView.addSubview(card)
        self.card.addSubview(eventName)
        self.card.addSubview(eventImage)
        self.card.addSubview(playImage)
        
        
        
        card.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(contentView)
            make.height.equalTo(contentView)
        }
        
        
        eventImage.contentMode = UIViewContentMode.scaleAspectFill
        eventImage.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(contentView).multipliedBy(0.5)
            make.left.top.bottom.equalTo(contentView)
        }
        
        eventImage.clipsToBounds = true
        
        
        eventName.snp.makeConstraints { (make) in
            make.left.equalTo(eventImage.snp.right).offset(5)
            make.width.equalTo(contentView).multipliedBy(0.5).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10))
            make.height.equalTo(contentView)
        }
        
        
        playImage.image = UIImage(named: "ic_play_circle_filled")
        playImage.alpha = 0.0
        
        playImage.snp.makeConstraints { (make) in
            make.center.equalTo(eventImage)
            make.width.height.equalTo(60)
        }
        
        eventName.isUserInteractionEnabled = false
        
        eventName.font = UIFont.systemFont(ofSize: 14)
        eventName.textColor = UIColor.black
        eventName.isEditable = false
        eventName.isSelectable = false
        
        eventName.isUserInteractionEnabled = false
        eventImage.isUserInteractionEnabled = false
        card.isUserInteractionEnabled = false
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if(highlighted) {
            self.backgroundColor = Colors.bg
        } else {
            self.backgroundColor =  Colors.bg
        }
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(5, 10, 5, 10))
        
    }
    
}
