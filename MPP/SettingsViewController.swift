import Foundation
import UIKit


class SettingsViewController : UIViewController {
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Colors.bg
    
        let innerViewController = AppPageTabBarController()
        self.addChildViewController(innerViewController)
        self.view.addSubview(innerViewController.view)
       
        innerViewController.didMove(toParentViewController: self)
    }
}
